<?php
/**
 * Created 11.01.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 * Plugin Name: JWA WP Locator
 * Plugin URI: https://www.justwebagency.com/
 * Description: Allows you to display shops, real estate, and more on the map
 * Author: Alex L
 * Author URI: https://gitlab.com/AlsconWeb
 * Version: 1.0.0
 * Domain Path: /languages
 * Since: 1.0
 * Requires WordPress Version at least: 5.1
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 * Text Domain:jwa_locator
 */

namespace JWA_Locator;

use JWA_Locator\CommentForm\jwaCommentForm;
use JWA_Locator\CPT\jwaCTP;
use JWA_Locator\MapAndFilter\mainQuery;
use JWA_Locator\metaField\jwaCityField;
use JWA_Locator\Rest\jwaLocationRESTApi;
use JWA_Locator\Settings\jwaSettingsField;
use JWA_Locator\Settings\jwaSettingsPage;

if ( ! defined( 'ABSPATH' ) ) {
	die ( 'Silly human what are you doing here' );
}

class jwaWPLocator {
	private $ctp;
//	private $matabox;
//	private $filter;
	private $rest;
	private $settings;
	private $settingsField;
//	private $style;
	private $commnet;
	private $db;
	private $mainQuery;
	private $cityTaxanomy;
	
	
	public function __construct () {
		
		
		define( 'JWA_LOCATION_VERSION', '1.0.0' );
		define( 'JWA_LOCATION_PLUGIN_DIR', untrailingslashit( plugin_dir_path( __FILE__ ) ) );
		define( 'JWA_LOCATION_PLUGIN_URL', untrailingslashit( plugins_url( basename( plugin_dir_path( __FILE__ ) ), basename( __FILE__ ) ) ) );
		define( 'JWA_LOCATION_POST_TYPE', 'locator' );
		define( 'JWA_LOCATION_POST_TYPE_MENU_NAME', 'Location' );
		define( 'JWA_LOCATION_ARCHIVE_SLUG', 'locations' );
		define( 'JWA_LOCATION_GOOGLE_KEY', 'AIzaSyAai79H6_p57ZUCgjc6EUFCuIbr_V6DQB4' );
		define( 'JWA_REST_NAMESPACE', 'jwa-locator/v1' );
		define( 'JWA_REST_TOKEN', 'xWgbfKYZvX6aEFvK' );
		
		require_once JWA_LOCATION_PLUGIN_DIR . '/vendor/autoload.php';
		
		add_action( 'init', [ $this, 'init' ] );
		add_action( 'admin_init', [ $this, 'adminInit' ] );
	}
	
	/**
	 * Get control version file
	 *
	 * @param string $src
	 *
	 * @return false|int
	 */
	private function versionFile ( string $src ) {
		return filemtime( JWA_LOCATION_PLUGIN_DIR . $src );
	}
	
	/**
	 * Init Plugin
	 */
	public function init (): void {
		global $wpdb;
		$this->db           = $wpdb;
		$this->ctp          = new jwaCTP( JWA_LOCATION_POST_TYPE, JWA_LOCATION_POST_TYPE_MENU_NAME, JWA_LOCATION_ARCHIVE_SLUG );
		$this->rest         = new jwaLocationRESTApi();
		$this->commnet      = new jwaCommentForm();
		$this->mainQuery    = new mainQuery();
		$this->cityTaxanomy = new jwaCityField();
		$this->settings     = new jwaSettingsPage();
		$this->addTableRating();
		$this->addTableLocation();


//		add_image_size( 'car_card', 155, 136, [ 'top', 'center' ] );
		
		// add script and style
		add_action( 'wp_enqueue_scripts', [ $this, 'addScript' ] );
		add_action( 'admin_enqueue_scripts', [ $this, 'addAdminScript' ] );
	}
	
	/**
	 * Add Script and Style
	 */
	public function addScript (): void {
		if ( is_singular( JWA_LOCATION_POST_TYPE ) || is_archive( JWA_LOCATION_POST_TYPE ) ) {
			wp_enqueue_script( 'bootstrap', JWA_LOCATION_PLUGIN_URL . '/assets/js/admin/metabox/bootstrap.min.js', [ 'jquery' ],
				$this->versionFile( '/assets/js/admin/metabox/bootstrap.min.js' ),
				true );
			
			wp_enqueue_script( 'map', JWA_LOCATION_PLUGIN_URL . '/assets/js/frontend/map.js', [
				'jquery',
				'g-map',
			], $this->versionFile( '/assets/js/frontend/map.js' ) );
			
			wp_enqueue_script( 'jwa-location-main', JWA_LOCATION_PLUGIN_URL . '/assets/js/frontend/main.js', [
				'jquery',
				'map',
			], $this->versionFile( '/assets/js/frontend/main.js' ) );
			wp_enqueue_script( 'jwa-location-common', JWA_LOCATION_PLUGIN_URL . '/assets/js/frontend/common.js', [
				'slick',
			], $this->versionFile( '/assets/js/frontend/common.js' ) );
			wp_enqueue_script( 'slick', JWA_LOCATION_PLUGIN_URL . '/assets/js/frontend/slick.min.js', [
				'jquery',
			], $this->versionFile( '/assets/js/frontend/slick.min.js' ) );
			wp_enqueue_script( 'chart', JWA_LOCATION_PLUGIN_URL . '/assets/js/frontend/chart.js', [
				'jquery',
			], $this->versionFile( '/assets/js/frontend/chart.js' ) );
			
			
			wp_enqueue_script( 'g-map', "//maps.googleapis.com/maps/api/js?key=" . JWA_LOCATION_GOOGLE_KEY . '&libraries=places&language=en', [ 'jquery' ],
				'1.0.0',
				true );
			
			wp_enqueue_style( 'bootstrap', JWA_LOCATION_PLUGIN_URL . '/assets/css/bootstrap-grid-3.3.1.min.css', '', $this->versionFile( '/assets/css/bootstrap-grid-3.3.1.min.css' ) );
			wp_enqueue_style( 'main', JWA_LOCATION_PLUGIN_URL . '/assets/css/main.css', '', $this->versionFile( '/assets/css/main.css' ) );
			wp_enqueue_style( 'slick', JWA_LOCATION_PLUGIN_URL . '/assets/css/slick.css', '', $this->versionFile( '/assets/css/slick.css' ) );
			wp_enqueue_style( 'slick-theme', JWA_LOCATION_PLUGIN_URL . '/assets/css/slick-theme.css', '', $this->versionFile( '/assets/css/slick-theme.css' ) );
			
			wp_localize_script( 'map', 'jwaLocation', [
				'ajax' => admin_url( 'admin-ajax.php' ),
				'key'  => JWA_LOCATION_GOOGLE_KEY,
			] );
		}
		
		wp_enqueue_script( 'sweetalert2', "//cdn.jsdelivr.net/npm/sweetalert2@10", [ 'jquery' ],
			'10.0.0',
			true );
	}
	
	/**
	 * Creates a table for ratings
	 */
	public function addTableRating (): void {
		$prefix          = $this->db->base_prefix;
		$rating          = $this->db->get_results( "SHOW TABLES LIKE '{$prefix}jwa_rating_location'" );
		$charset_collate = $this->db->get_charset_collate();
		
		if ( ! $rating ) {
			$sql = "CREATE TABLE `{$prefix}jwa_rating_location` ( `id` BIGINT NOT NULL AUTO_INCREMENT , `post_id` BIGINT NULL , `rating` INT NULL , `comment_id` BIGINT NULL , `status` TEXT NULL , `date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, `date_post` DATE NULL,  `avatar` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL, PRIMARY KEY (`id`)) $charset_collate;";
			require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
			dbDelta( $sql );
		}
	}
	
	/**
	 * Creates a table for location
	 */
	public function addTableLocation (): void {
		$prefix          = $this->db->base_prefix;
		$rating          = $this->db->get_results( "SHOW TABLES LIKE '{$prefix}jwa_location'" );
		$charset_collate = $this->db->get_charset_collate();
		
		if ( ! $rating ) {
			$sql = "CREATE TABLE `{$prefix}jwa_location` ( `id` BIGINT NOT NULL AUTO_INCREMENT, `post_id` BIGINT NULL, `address` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL, `postal_code` VARCHAR(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL, `country` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL, `province` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL, `lat` FLOAT NULL, `lng` FLOAT NULL, PRIMARY KEY  (`id`)) $charset_collate;";
			require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
			dbDelta( $sql );
		}
	}
	
	public function adminInit (): void {
		$this->settingsField = new jwaSettingsField();
		
	}
	
	/**
	 * Add JS\CSS to admin panel
	 *
	 * @param $pageName
	 */
	public function addAdminScript ( $pageName ): void {
		if ( $pageName === 'admin_page_location-settings' ) {
			wp_enqueue_script( 'token-generate', JWA_LOCATION_PLUGIN_URL . '/assets/js/admin/settingsPage/main.js', [ 'jquery' ],
				$this->versionFile( '/assets/js/admin/settingsPage/main.js' ),
				true );
		}
	}
}

new jwaWPLocator();