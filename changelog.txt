v1.0.0a
=========================
1 implemented automatic setting of the city and province by google autocomplete
2 add USA state
3 fix Open until time
4 fix about text formatting
5 fix Close time
6 add location by IP user
7 fix rest update location
8 small fix front-end filter map
9 fix generation url by address

v1.0.0b
=========================
1 fix distance by search from address
2 fix area taxonomy template
3 add function getCoordinateByAddress
4 fix output location by address
5 add settings page
6 fix delete gallery image
7 fix output if not gallery
