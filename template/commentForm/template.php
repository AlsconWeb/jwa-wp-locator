<?php
/**
 * Created 11.02.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */

?>

<form class="reviews-form dfr" id="comment_dis" method="post">
	<h3><?php _e( 'Write a Review', 'jwa_locator' ); ?></h3>
	<p><?php _e( 'Rate Dispensary', 'jwa_locator' ); ?></p>
	<div class="rating dfr">
		<ul class="dfr" data-rating="4">
			<li class="icon-star" data-index="1"></li>
			<li class="icon-star" data-index="2"></li>
			<li class="icon-star" data-index="3"></li>
			<li class="icon-star" data-index="4"></li>
			<li class="icon-star" data-index="5"></li>
		</ul>
	</div>
	<?php if ( ! is_user_logged_in() ): ?>
		<div class="input">
			<input type="text" id="name" placeholder="Name" name="jwa_location_review[user_name]" required>
		</div>
		<div class="input">
			<input type="email" id="email" placeholder="Email" name="jwa_location_review[user_email]" required>
		</div>
	<?php endif; ?>
	<div class="textarea">
		<textarea placeholder="Comment..." name="jwa_location_review[user_comment]"></textarea>
	</div>
	<div class="input">
		<input type="hidden" name="jwa_location_review[rate]" id="rate">
		<input type="hidden" name="jwa_location_review[post_id]" id="post_id" value="<?php echo get_the_ID(); ?>">
		<input type="hidden" name="jwa_location_review[user_id]" id="user_id" value="<?php echo get_current_user_id(); ?>">
		<input type="hidden" name="action" value="jwa_location_reviews">
		
		<input class="button" type="submit" value="<?php _e( 'Submit', 'jwa_locator' ); ?>">
	</div>
</form>
