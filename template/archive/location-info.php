<?php
/**
 * Created 25.02.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */
$id             = get_the_ID();
$helpers        = new \JWA_Locator\Helpers\jwaPostData();
$general        = $helpers->getInLocationTable( $id );
$commentHelpers = new \JWA_Locator\Helpers\jwaLocationReview();
$businessTime   = new \JWA_Locator\Helpers\jwaLocationTime( $id );
?>

<div class="results-description dfr">
	<?php if ( has_post_thumbnail( $id ) ):the_post_thumbnail( [ 150, 150 ] ); ?>
	<?php else: ?>
		<img src="https://via.placeholder.com/150x150.png?text=No+Image" alt="No Image">
	<?php endif; ?>
	<div class="description">
		<h3><?php the_title(); ?></h3>
		<p class="address"><?php echo( ! empty( $general['address'] ) ? $general['address'] : '' ); ?></p>
		<div class="rating dfr">
			<ul class="dfr"
			    data-rating="<?php echo round( $commentHelpers->getStarRating( $id ), 0, PHP_ROUND_HALF_DOWN ) ?>">
				<li class="icon-star"></li>
				<li class="icon-star"></li>
				<li class="icon-star"></li>
				<li class="icon-star"></li>
				<li class="icon-star"></li>
			</ul>
			<p>(<?php echo $commentHelpers->getCountComment( $id ) ?> - <?php _e( 'reviews', 'jwa_locator' ); ?>)</p>
		</div>
		<?php if ( $businessTime->openCurrentDay() ): ?>
			<p class="status icon-clock"><?php _e( 'Open until', 'jwa_locator' ); ?>
				<span> <?php echo $businessTime->openCurrentDay(); ?></span>
			</p>
		<?php else: ?>
			<p class="status icon-lock"><?php _e( 'Closed now', 'jwa_locator' ); ?></p>
		<?php endif; ?>
	</div>
	<h3><?php _e( 'Business Hours', 'jwa_locator' ); ?></h3>
	<?php if ( $businessTime->getTimeOpen() ) : ?>
		<ul class="business-hours">
			<?php foreach ( $businessTime->getTimeOpen() as $key => $day ): ?>
				<?php if ( $day['current_day'] ): ?>
					<li><strong><?php echo $day['day'] ?><span><?php echo $day['open'] ?></span></strong></li>
				<?php else: ?>
					<li><?php echo $day['day'] ?><span><?php echo $day['open'] ?></span></li>
				<?php endif; ?>
			<?php endforeach; ?>
		
		</ul>
	<?php else: ?>
		<ul class="business-hours no-info">
			<li><?php _e( 'Not Information', 'jwa_locator' ); ?></li>
		</ul>
	<?php endif; ?>
	<div class="buttons dfr">
		<a class="button" href="<?php the_permalink(); ?>"><?php _e( 'More Info', 'jwa_locator' ); ?></a>
		<a class="button" rel="nofollow noopener"
		   target="_blank"
		   href="https://maps.google.com/maps?daddr=(<?php echo $general['lat']; ?>, <?php echo $general['lng']; ?>)">
			<?php _e( 'Get Directions', 'jwa_locator' ); ?>
		</a>
	</div>
</div>
