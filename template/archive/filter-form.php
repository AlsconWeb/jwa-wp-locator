<?php
/**
 * Created 01.03.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */

$caregoryes = get_terms( [ 'taxonomy' => 'location-category', 'hide_empty' => false, ] );
$tags       = get_terms( [ 'taxonomy' => 'tag-locations', 'hide_empty' => false, ] );
$objTerm    = get_queried_object();

if ( isset( $params['category'] ) && is_string( $params['category'] ) ) {
	$params['category'] = [ $params['category'] ];
}

if ( is_string( $params['address'] ) ) {
	$params['address'] = [ $params['address'] ];
}
?>

<form class="filters dfr" action="<?php echo admin_url( 'admin-post.php' ); ?>" method="post"
      id="filter-top">
	
	<input type="hidden" name="jwa_location[lat]" id="jwa-location-lat" value="">
	<input type="hidden" name="jwa_location[lng]" id="jwa-location-lng" value="">
	<input type="hidden" name="jwa_location[distance]" id="jwa-location-distance"
	       value="<?php echo( empty( $objTerm->parent ) ? 1700 : 100 ) ?>">
	<input type="hidden" name="jwa_location[city]" id="jwa-location-city"
	       value="<?php echo( ! empty( $objTerm->parent ) ? $objTerm->name : '' ) ?>">
	<input type="hidden" name="jwa_location[province]" id="jwa-location-province"
	       value="<?php echo( empty( $objTerm->parent ) ? $objTerm->slug : get_term_by( 'id', $objTerm->parent, 'area' )->slug ) ?>">
	<input type="hidden" name="action" value="jwa_location_filter"/>
	<div class="search">
		<input type="search" placeholder="<?php _e( 'Enter a Location' ); ?>" name="jwa_location[filter][address]"
		       id="jwa-location-address"
		       value="<?php echo( ! empty( $params['address'] ) ? urldecode( $params['address'][0] ) : '' ) ?>">
		<button class="icon-search"></button>
	</div>
	<?php if ( ! empty( $caregoryes ) && ! is_wp_error( $caregoryes ) ): ?>
		<div class="categories dfr">
			<?php foreach ( $caregoryes as $category ): ?>
				<div class="checkbox-button">
					<input id="<?php echo $category->slug; ?>-button" name="jwa_location[filter][category][]" type="checkbox"
					       class="<?php echo( ! empty( $params['category'] ) && in_array( $category->slug, $params['category'] ) ? 'checked' : '' ) ?>"
					       value="<?php echo $category->slug; ?>" data-id="<?php echo $category->term_id ?>"
						<?php echo( ! empty( $params['category'] ) && in_array( $category->slug, $params['category'] ) ? 'checked' : '' ) ?>
					>
					<label class="icon-<?php echo $category->slug; ?>" for="<?php echo $category->slug; ?>-button"><?php echo
						$category->name; ?></label>
				</div>
			<?php endforeach; ?>
		</div>
	<?php endif; ?>
	<?php if ( ! empty( $tags ) ):
		$i = 0;
		foreach ( $tags as $tag ):
			?>
			<div class="checkbox-button">
				<input id="<?php echo $tag->slug ?>-button" type="checkbox" name="jwa_location[filter][tags][]"
				       class="<?php echo( ! empty( $params['tags'] ) && in_array( $tag->slug, $params['tags'] ) ? 'checked' : '' ) ?>"
				       value="<?php echo $tag->slug ?>" data-id="<?php echo $tag->term_id ?>"
					<?php echo( ! empty( $params['tags'] ) && in_array( $tag->slug, $params['tags'] ) ? 'checked' : '' ) ?>
				>
				<label for="<?php echo $tag->slug ?>-button"><?php echo $tag->name ?></label>
			</div>
			<?php if ( $i == 2 ) {
			break;
		}
			$i ++ ?>
		<?php endforeach; endif; ?>
	<div class="select">
		<p class="icon-filters"><?php _e( 'Filters', 'jwa_locator' ); ?></p>
		<ul class="select-options">
			<?php if ( ! empty( $caregoryes ) ): ?>
				<?php foreach ( $caregoryes as $category ): ?>
					<li>
						<div class="checkbox">
							<input id="<?php echo $category->slug; ?>"
							       class="<?php echo( ! empty( $params['category'] ) && in_array( $category->slug, $params['category'] ) ? 'checked' :
								       '' ) ?>" name="jwa_location[filter][category][]" type="checkbox"
							       value="<?php echo $category->slug; ?>" data-id="<?php echo $category->term_id ?>"
								<?php echo( ! empty( $params['category'] ) && in_array( $category->slug, $params['category'] ) ? 'checked' : '' ) ?>
							>
							<label class="<?php echo $category->slug; ?> icon-check-mark"
							       for="<?php echo $category->slug; ?>"><?php echo
								$category->name; ?></label>
						</div>
					</li>
				<?php endforeach; ?>
			<?php endif; ?>
			<?php if ( ! empty( $tags ) ):
				foreach ( $tags as $tag ):
					?>
					<li>
						<div class="checkbox">
							<input id="<?php echo $tag->slug ?>" type="checkbox" name="jwa_location[filter][tags][]"
							       class="<?php echo( ! empty( $params['tags'] ) && in_array( $tag->slug, $params['tags'] ) ? 'checked' : '' ) ?>"
							       value="<?php echo $tag->slug ?>" data-id="<?php echo $tag->term_id ?>"
								<?php echo( ! empty( $params['tags'] ) && in_array( $tag->slug, $params['tags'] ) ? 'checked' : '' ) ?>
							>
							<label for="<?php echo $tag->slug ?>" class="icon-check-mark"><?php echo $tag->name ?></label>
						</div>
					</li>
				<?php endforeach; endif; ?>
		</ul>
		<div class="select-options mobile">
			<ul class="nav-tabs dfr">
				<li><a href="#filters" data-toggle="tab"><?php _e( 'Filters', 'jwa_locator' ); ?></a></li>
				<li><a href="#sort" data-toggle="tab"><?php _e( 'Sort', 'jwa_locator' ); ?></a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="filters">
					<?php if ( ! empty( $caregoryes ) ): ?>
						<?php foreach ( $caregoryes as $category ): ?>
							<div class="checkbox icon-<?php echo $category->slug; ?>">
								<input id="<?php echo $category->slug; ?>-mob" name="jwa_location[filter][category][]" type="checkbox"
								       value="<?php echo $category->slug; ?>" data-id="<?php echo $category->term_id ?>"
								       class="<?php echo( ! empty( $params['category'] ) && in_array( $category->slug, $params['category'] ) ? 'checked' : '' ) ?>"
									<?php echo( ! empty( $params['category'] ) && in_array( $category->slug, $params['category'] ) ? 'checked' : '' ) ?>
								>
								<label class="<?php echo $category->slug; ?>-mob"
								       for="<?php echo $category->slug; ?>-mob"><?php echo
									$category->name; ?></label>
							</div>
						<?php endforeach; ?>
					<?php endif; ?>
					<?php if ( ! empty( $tags ) ):
						foreach ( $tags as $tag ):
							?>
							<div class="checkbox">
								<input id="<?php echo $tag->slug ?>-mob" type="checkbox" name="jwa_location[filter][tags][]"
								       class="<?php echo( ! empty( $params['tags'] ) && in_array( $tag->slug, $params['tags'] ) ? 'checked' : '' ) ?>"
								       value="<?php echo $tag->slug ?>" data-id="<?php echo $tag->term_id ?>"
									<?php echo( ! empty( $params['tags'] ) && in_array( $tag->slug, $params['tags'] ) ? 'checked' : '' ) ?>
								>
								<label for="<?php echo $tag->slug ?>-mob"><?php echo $tag->name ?></label>
							</div>
						<?php endforeach; endif; ?>
				</div>
				<div class="tab-pane" id="sort">
					<div class="radio-button">
						<input id="popular" type="radio" name="jwa_location[filter][sort]" value="popular">
						<label for="popular"><?php _e( 'By Popular', 'jwa_locator' ) ?></label>
					</div>
					<div class="radio-button">
						<input id="rating" type="radio" name="jwa_location[filter][sort]" value="rating">
						<label for="rating"><?php _e( 'By Rating', 'jwa_locator' ); ?></label>
					</div>
					<div class="radio-button">
						<input id="a-z" type="radio" name="jwa_location[filter][sort]" value="name_asc">
						<label for="a-z"><?php _e( 'Name A-Z', 'jwa_locator' ); ?></label>
					</div>
					<div class="radio-button">
						<input id="z-a" type="radio" name="jwa_location[filter][sort]" value="name_desc">
						<label for="z-a"><?php _e( 'Name Z-A', 'jwa_locator' ); ?></label>
					</div>
				</div>
			</div>
			<input class="button" type="submit" value="<?php _e( 'Apply Filters', 'jwa_locator' ); ?>">
		</div>
	</div>
</form>
