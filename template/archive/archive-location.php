<?php
/**
 * Created 02.03.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */

/**
 * @TODO Нужно убрать нотисы
 */

get_header();

$limit           = 10;
$termCity        = new \JWA_Locator\Helpers\jwaUserInfo( $_SERVER['REMOTE_ADDR'] );
$term            = $termCity->getTermByIP();
$cityData        = $termCity->getDefaultCityByUser();
$locationArchive = new  \JWA_Locator\MapAndFilter\mainQuery();
$params          = $locationArchive->parseURLQuery( get_query_var( 'filter' ) );
if ( ! empty( $lat ) && ! empty( $lng ) ) {
	if ( ! empty( $params['address'] ) ) {
		$coordinate = $locationArchive->getCoordinateByAddress( $params['address'][0] );
		$location   = $locationArchive->getLocationByCoordinate( $coordinate['lat'], $coordinate['lng'], 50 );
	} else {
		$location = $locationArchive->getLocationByCoordinate( $lat, $lng, 50 );
	}
} else {
	if ( ! empty( $params['address'] ) ) {
		$coordinate = $locationArchive->getCoordinateByAddress( $params['address'][0] );
		$lat        = $coordinate['lat'];
		$lng        = $coordinate['lng'];
		$location   = $locationArchive->getLocationByCoordinate( $coordinate['lat'], $coordinate['lng'], 2000 );
		
	}
	$location = [];
}

if ( empty( $params ) ) {
	$arg = [
		'post_type'      => JWA_LOCATION_POST_TYPE,
		'posts_per_page' => $limit,
		'order'          => 'DESC',
		'post__in'       => ( ! empty( $location['IDs'] ) ? $location['IDs'] : [] ),
		'orderby'        => 'post__in',
		'post_status'    => 'publish',
		'tax_query'      => [
			'relation' => "IN",
			[
				'taxonomy'         => 'area',
				'field'            => 'id',
				'terms'            => [ $cityData['city_id'] ],
				'include_children' => false,
			],
		],
	];
} else {
	
	$arg = $locationArchive->getQueryArg( $params, ( ! empty( $location['IDs'] ) ? $location['IDs'] : [] ), $termCity->term_id );
}

$query = new WP_Query( $arg );

?>
<section class="jwa-map-list">
	<div class="dispensaries">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<?php include_once JWA_LOCATION_PLUGIN_DIR . '/template/archive/filter-form.php' ?>
					<div class="map-block">
						<div id="map" data-lat="<?php echo( ! empty( $lat ) ? $lat : $cityData['lat'] ) ?>"
						     data-lng="<?php echo( ! empty( $lng ) ? $lng : $cityData['lng'] ) ?>"></div>
						<div class="results">
							<div class="results-block">
								<div class="results-head dfr">
									<p><?php _e( 'Showing Results', 'jwa_locator' ); ?> <span
											id="start"><?php echo $query->post_count ?></span> - <span
											class="end"><?php echo
											$query->found_posts; ?></span></p>
									<div class="sort-filter">
										<!--										<a class="icon-up" href="#">By Rating</a>-->
										<!--										<ul>-->
										<!--											<li><a href="#">By Popular</a></li>-->
										<!--											<li><a href="#">By Rating</a></li>-->
										<!--											<li><a href="#">Name A-Z</a></li>-->
										<!--											<li><a href="#">Name Z-A</a></li>-->
										<!--										</ul>-->
									</div>
								</div>
								<div class="results-body">
									<?php
									if ( $query->have_posts() ):
										while ( $query->have_posts() ):$query->the_post();
											include JWA_LOCATION_PLUGIN_DIR . '/template/archive/location-item.php';
											?>
										<?php endwhile;
										wp_reset_postdata(); ?>
									<?php else: ?>
										<div class="no-found dfr">
											<div class="description"><p><?php _e( 'No found, they will appear soon', 'jwa_locator' ); ?></p>
											</div>
										</div>
									<?php endif; ?>
								</div>
							</div>
							<div class="results-dispensaries">
								<div class="results-head dfr">
									<a class="icon-arrow-left back" href="#">
										<?php _e( 'Back to Results', 'Back to Results' ); ?></a></div>
								<div class="results-description dfr">
									<img src="<?php echo JWA_LOCATION_PLUGIN_URL ?>/assets/img/no-image.jpg" alt="">
									<div class="description">
										<h3>Central Cannabis</h3>
										<p class="address">1255 Bay Street, Unit #702 Toronto, Ontario</p>
										<div class="rating dfr">
											<ul class="dfr" data-rating="3">
												<li class="icon-star"></li>
												<li class="icon-star"></li>
												<li class="icon-star"></li>
												<li class="icon-star"></li>
												<li class="icon-star"></li>
											</ul>
											<p>(131 reviews)</p>
										</div>
										<p class="status icon-lock">Permanently Closed</p>
									</div>
									<h3>Business Hours</h3>
									<ul class="business-hours">
										<li>Monday<span>9:00am-6:00pm</span></li>
										<li>Tuesday<span>9:00am-6:00pm</span></li>
										<li><strong>Wednesday<span>9:00am-6:00pm</span></strong></li>
										<li>Thursday<span>9:00am-6:00pm</span></li>
										<li>Friday<span>9:00am-6:00pm</span></li>
										<li>Saturday<span>Closed</span></li>
										<li>Sunday<span>Closed</span></li>
									</ul>
									<div class="buttons dfr"><a class="button" href="#">More Info</a><a class="button get-directions"
									                                                                    href="#">Get Directions</a></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="nearest">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<h2 class="title">
						<span>
							<?php _e( 'Information on ', 'jwa_locator' );
							echo $term->name; ?>
						</span>
					</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<?php if ( ! empty( $term->description ) ): ?>
						<div class="description">
							<?php echo term_description( $term, 'area' ); ?>
						</div>
					<?php else: ?>
						<div class="description">
							<?php _e( 'Description will bi coming soon', 'jwa_locator' ); ?>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>

