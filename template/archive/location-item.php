<?php
/**
 * Created 25.02.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */

$id             = get_the_ID();
$helpers        = new \JWA_Locator\Helpers\jwaPostData();
$general        = $helpers->getInLocationTable( $id );
$commentHelpers = new \JWA_Locator\Helpers\jwaLocationReview();
$businessTime   = new \JWA_Locator\Helpers\jwaLocationTime( $id );
$category       = wp_get_post_terms( $id, 'location-category' );
?>
<div class="item dfr" data-lat="<?php echo $general['lat']; ?>" data-lng="<?php echo $general['lng']; ?>"
     data-type="<?php echo( ! empty( $category[0] ) ? $category[0]->slug : 'dispensaries' ) ?>-marker"
     data-id="<?php echo $id; ?>">
	<?php if ( has_post_thumbnail( $id ) ):the_post_thumbnail( [ 150, 150 ] ); ?>
	<?php else: ?>
		<img src="https://via.placeholder.com/150x150.png?text=No+Image" alt="No Image">
	<?php endif; ?>
	<div class="description">
		<h3><?php the_title(); ?></h3>
		<p class="address"><?php echo( ! empty( $general['address'] ) ? $general['address'] : '' ); ?></p>
		<div class="rating dfr">
			<ul class="dfr"
			    data-rating="<?php echo round( $commentHelpers->getStarRating( $id ), 0, PHP_ROUND_HALF_DOWN ) ?>">
				<li class="icon-star"></li>
				<li class="icon-star"></li>
				<li class="icon-star"></li>
				<li class="icon-star"></li>
				<li class="icon-star"></li>
			</ul>
			<p>(<?php echo $commentHelpers->getCountComment( $id ) ?> - <?php _e( 'reviews', 'jwa_locator' ); ?>)</p>
		</div>
		<?php if ( $businessTime->openCurrentDay() ): ?>
			<p class="status icon-clock"><?php _e( 'Open until', 'jwa_locator' ); ?>
				<span> <?php echo $businessTime->openCurrentDay(); ?></span>
			</p>
		<?php else: ?>
			<p class="status icon-lock"><?php _e( 'Closed now', 'jwa_locator' ); ?></p>
		<?php endif; ?>
		<?php if ( isset( $location['distance_location'] ) ): ?>
			<p class="status icon-marker"><?php _e( 'Distance', 'jwa_locator' ); ?>
				<span><?php echo( isset( $location['distance_location'] ) && array_key_exists( $id, $location['distance_location'] ) ? round( $location['distance_location'][ $id ]['distance'], 2 ) . 'km' : '' ); ?></span>
			</p>
		<?php endif; ?>
	</div>
</div>
