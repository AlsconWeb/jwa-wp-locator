<?php
/**
 * Created 10.02.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */

get_header();

$id              = get_the_ID();
$helpers         = new \JWA_Locator\Helpers\jwaPostData();
$postMeta        = $helpers->getMetaField( [ 'phone', 'web', 'about', 'gallery', 'soc' ], $id );
$businessTime    = new \JWA_Locator\Helpers\jwaLocationTime( $id );
$commentHelpers  = new \JWA_Locator\Helpers\jwaLocationReview();
$general         = $helpers->getInLocationTable( $id );
$locationArchive = new  \JWA_Locator\MapAndFilter\mainQuery();
?>
<section class="jwa-map-list">
	<div class="dispensary">
		<div class="container">
			<?php if ( function_exists( 'bcn_display' ) ):
				/**
				 * @TODO Нужно добавить стандартные хлебные крошки
				 */
				?>
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div
							class="breadcrumbs"><?php bcn_display( $return = false, $linked = true, $reverse = false, $force = false ); ?></div>
					</div>
				</div>
			<?php endif; ?>
			<div class="row">
				<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
					<div class="item dfr" data-lat="<?php echo $general['lat']; ?>"
					     data-lng="<?php echo $general['lng']; ?>"
					     data-type="<?php echo wp_get_post_terms( $id, 'location-category' )[0]->slug ?>-marker">
						<?php if ( has_post_thumbnail( $id ) ): the_post_thumbnail(); ?>
						<?php else: ?>
							<img src="https://via.placeholder.com/150x150.png?text=No+Image" alt="No Image">
						<?php endif; ?>
						<div class="description">
							<h3><?php the_title(); ?></h3>
							<div class="rating dfr">
								<ul class="dfr"
								    data-rating="<?php echo round( $commentHelpers->getStarRating( $id ), 0, PHP_ROUND_HALF_DOWN ) ?>">
									<li class="icon-star"></li>
									<li class="icon-star"></li>
									<li class="icon-star"></li>
									<li class="icon-star"></li>
									<li class="icon-star"></li>
								</ul>
								<p>(<?php echo $commentHelpers->getCountComment( $id ) ?> reviews)</p>
							</div>
							<p class="address"><?php echo( ! empty( $general['address'] ) ? $general['address'] : '' ); ?></p>
							<?php if ( ! empty( $postMeta['phone'] ) ): ?>
								<a
									class="phone icon-phone"
									href="tel:<?php echo $postMeta['phone']; ?>"><?php echo $postMeta['phone']; ?></a>
							<?php endif; ?>
							<?php if ( ! empty( $postMeta['web'] ) ): ?>
								<a class="web icon-planet" href="<?php echo $postMeta['web']; ?>" target="_blank"
								   rel="nofollow noopener"><?php echo $postMeta['web']; ?></a>
							<?php endif; ?>
							
							<?php if ( $businessTime->openCurrentDay() ): ?>
								<p class="status icon-clock"><?php _e( 'Open until', 'jwa_locator' ); ?>
									<span> <?php echo $businessTime->openCurrentDay(); ?></span>
								</p>
							<?php else: ?>
								<p class="status icon-lock"><?php _e( 'Closed now', 'jwa_locator' ); ?></p>
							<?php endif; ?>
							
							<div class="buttons dfr">
								<a class="button" href="tel:<?php echo $postMeta['phone']; ?>"><?php _e( 'Call', 'jwa_locator' ); ?></a>
								<a class="button" rel="nofollow noopener"
								   target="_blank"
								   href="https://maps.google.com/maps?daddr=(<?php echo $general['lat']; ?>, <?php echo $general['lng']; ?>)">
									<?php _e( 'Get Directions', 'jwa_locator' ); ?>
								</a>
							</div>
							<div class="share dfr">
								<div class="icon-share"></div>
								<ul class="soc">
									<?php if ( ! empty( $postMeta['soc']['fb'] ) ): ?>
										<li><a class="icon-facebook"
										       href="<?php echo $postMeta['soc']['fb']; ?>"></a>
										</li>
									<?php endif; ?>
									<?php if ( ! empty( $postMeta['soc']['tw'] ) ): ?>
										<li><a class="icon-twitter" href="<?php echo $postMeta['soc']['tw']; ?>"></a></li>
									<?php endif; ?>
									<?php if ( ! empty( $postMeta['soc']['in'] ) ): ?>
										<li><a class="icon-linkedin" href="<?php echo $postMeta['soc']['in']; ?>"></a></li>
									<?php endif; ?>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 dfr">
					<h3><?php _e( 'Review Summary', 'jwa_locator' ); ?></h3>
					<div class="chart"
					     data-percent="<?php echo $commentHelpers->getPercentToProgressBar( 5, $commentHelpers->getStarRating( $id ) ) ?>">
						<p><?php echo $commentHelpers->getStarRating( $id ) ?></p>
						<div class="rating dfr">
							<ul class="dfr"
							    data-rating="<?php echo round( $commentHelpers->getStarRating( $id ), 0, PHP_ROUND_HALF_DOWN ) ?>">
								<li class="icon-star"></li>
								<li class="icon-star"></li>
								<li class="icon-star"></li>
								<li class="icon-star"></li>
								<li class="icon-star"></li>
							</ul>
						</div>
					</div>
					<?php $ratingChart = $commentHelpers->getRatingChartData( $id ); ?>
					<ul class="progress-bars">
						<li class="dfr"><span>5 Stars</span>
							<div class="progress">
								<div class="progress-bar" role="progressbar"
								     aria-valuenow="<?php echo $ratingChart['reviews']['five']['percent'] ?>" aria-valuemin="0"
								     aria-valuemax="100"
								     style="width: <?php echo $ratingChart['reviews']['five']['percent'] ?>%;"></div>
							</div>
							<p><?php echo $ratingChart['reviews']['five']['star'] ?></p>
						</li>
						<li class="dfr"><span>4 Stars</span>
							<div class="progress">
								<div class="progress-bar" role="progressbar"
								     aria-valuenow="<?php echo $ratingChart['reviews']['three']['percent'] ?>" aria-valuemin="0"
								     aria-valuemax="100"
								     style="width: <?php echo $ratingChart['reviews']['three']['percent'] ?>%;"></div>
							</div>
							<p><?php echo $ratingChart['reviews']['four']['star'] ?></p>
						</li>
						<li class="dfr"><span>3 Stars</span>
							<div class="progress">
								<div class="progress-bar" role="progressbar"
								     aria-valuenow="<?php echo $ratingChart['reviews']['three']['percent'] ?>" aria-valuemin="0"
								     aria-valuemax="100"
								     style="width: <?php echo $ratingChart['reviews']['three']['percent'] ?>%;"></div>
							</div>
							<p><?php echo $ratingChart['reviews']['three']['star'] ?></p>
						</li>
						<li class="dfr"><span>2 Stars</span>
							<div class="progress">
								<div class="progress-bar" role="progressbar"
								     aria-valuenow="<?php echo $ratingChart['reviews']['two']['percent'] ?>" aria-valuemin="0"
								     aria-valuemax="100"
								     style="width: <?php echo $ratingChart['reviews']['two']['percent'] ?>%;"></div>
							</div>
							<p><?php echo $ratingChart['reviews']['two']['star'] ?></p>
						</li>
						<li class="dfr"><span>1 Stars</span>
							<div class="progress">
								<div class="progress-bar" role="progressbar"
								     aria-valuenow="<?php echo $ratingChart['reviews']['one']['percent'] ?>" aria-valuemin="0"
								     aria-valuemax="100"
								     style="width: <?php echo $ratingChart['reviews']['one']['percent'] ?>%;"></div>
							</div>
							<p><?php echo $ratingChart['reviews']['one']['star'] ?></p>
						</li>
					</ul>
				
				</div>
			</div>
		</div>
	</div>
	<div class="dispensary-description">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<ul class="nav nav-pills dispensary-nav dfr" id="tabs_menu" role="tablist">
						
						<li class="nav-item active" role="presentation">
							<a href="#business-hours" class="nav-link"
							   data-toggle="tab"><?php _e( 'Business Hours', 'jwa_locator' ); ?></a>
						</li>
						<li class="nav-item" role="presentation">
							<a href="#reviews" class="nav-link" data-toggle="tab"
							   aria-selected="true"><?php _e( 'Reviews', 'jwa_locator' ); ?>
								<span>(<?php echo $commentHelpers->getCountComment( $id ) ?>)</span></a>
						</li>
						<li class="nav-item" role="presentation"><a class="nav-link" href="#about"
						                                            data-toggle="tab"><?php _e( 'About', 'jwa_locator' ); ?></a>
						</li>
					
					</ul>
					
					<div class="tab-content">
						<div class="tab-pane active" id="business-hours">
							<?php
							/**
							 * @TODO Нужно добавить возможность закрыть полностью магазин
							 *       добавить класс icon-lock и data-title="Permanently Closed"
							 */
							?>
							<?php if ( $businessTime->getTimeOpen() ) : ?>
								<ul class="business-hours">
									<?php foreach ( $businessTime->getTimeOpen() as $key => $day ): ?>
										<?php if ( $day['current_day'] ): ?>
											<li><strong><?php echo $day['day'] ?><span><?php echo $day['open'] ?></span></strong></li>
										<?php else: ?>
											<li><?php echo $day['day'] ?><span><?php echo $day['open'] ?></span></li>
										<?php endif; ?>
									<?php endforeach; ?>
								
								</ul>
							<?php else: ?>
								<ul class="business-hours no-info">
									<li><?php _e( 'Not Information', 'jwa_locator' ); ?></li>
								</ul>
							<?php endif; ?>
							<?php if ( ! empty( $general['lat'] ) && ! empty( $general['lng'] ) ): ?>
								<div id="map" data-lat="<?php echo $general['lat']; ?>" data-lng="<?php echo $general['lng']; ?>"></div>
							<?php endif; ?>
						</div>
						<div class="tab-pane" id="reviews">
							<?php
							$commentForm = new \JWA_Locator\CommentForm\jwaCommentForm();
							echo $commentForm->getCommentForm();
							
							?>
							<?php if ( $commentHelpers->getCountComment( $id ) !== 0 ): ?>
								<div class="comment_block">
									<div class="review">
										<h2><?php _e( 'Reviews', 'jwa_locator' ); ?>
											<span>(<?php echo $commentHelpers->getCountComment( $id ) ?>)</span></h2>
										<?php comments_template(); ?>
									</div>
								</div>
							<?php endif; ?>
						</div>
						<div class="tab-pane" id="about">
							<div class="left-block">
								<h3><?php the_title(); ?></h3>
								<?php echo wpautop( $postMeta['about'] ); ?>
							</div>
							<div class="right-block">
								<h3><?php _e( 'Photos', 'jwa_locator' ); ?>
									(<?php echo( ! empty( $postMeta['gallery'] ) ? count( explode( ',',
										$postMeta['gallery'] ) ) : 0 ) ?>)</h3>
								<?php if ( ! empty( $postMeta['gallery'] ) && count( explode( ',', $postMeta['gallery'] ) ) > 1 ):
									$images = explode( ',', $postMeta['gallery'] );
									?>
									<div class="about-slider">
										<?php foreach ( $images as $image ): ?>
											<div class="item"><img src="<?php echo wp_get_attachment_image_url( $image, 'full' ) ?>"
											                       alt="<?php echo get_the_title( $image ) ?>"></div>
										<?php endforeach; ?>
									</div>
									<div class="about-slider-nav">
										<?php foreach ( $images as $image ): ?>
											<div class="item"><img src="<?php echo wp_get_attachment_image_url( $image, 'small' ) ?>"
											                       alt="<?php echo get_the_title( $image ) ?>"></div>
										<?php endforeach; ?>
									</div>
								<?php elseif ( ! empty( $postMeta['gallery'] ) ): ?>
									<img src="<?php echo wp_get_attachment_image_url( $postMeta['gallery'], 'full' ) ?>"
									     alt="<?php echo get_the_title( $postMeta['gallery'] ) ?>">
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="nearest">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<h2 class="title"><span><?php _e( 'Nearest Dispensaries', 'jwa' ); ?></span></h2>
				</div>
			</div>
			<div class="row">
				<?php
				$location = $locationArchive->getLocationByCoordinate( $general['lat'], $general['lng'], 50 );
				$city     = $helpers->getLocationCity( wp_get_post_terms( $id, 'area' ) );
				$arg      = [
					'post_type'      => JWA_LOCATION_POST_TYPE,
					'posts_per_page' => 3,
					'order'          => 'DESC',
					'post__in'       => ( ! empty( $location['IDs'] ) ? $location['IDs'] : [] ),
					'orderby'        => 'post__in',
					'post_status'    => 'publish',
//					'tax_query'      => [
//						'relation' => "IN",
//						[
//							'taxonomy'         => 'area',
//							'field'            => 'id',
//							'terms'            => [ $city->term_id ],
//							'include_children' => false,
//						],
//					],
				];
				$query    = new WP_Query( $arg );
				?>
				<?php if ( $query->have_posts() ):
					while ( $query->have_posts() ): $query->the_post();
						$nearID           = get_the_ID();
						$nearGeneral      = $helpers->getInLocationTable( $nearID );
						$nearBusinessTime = new \JWA_Locator\Helpers\jwaLocationTime( $nearID );
						?>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<a class="item dfr" href="<?php the_permalink(); ?>">
								<?php if ( has_post_thumbnail( $nearID ) ): the_post_thumbnail( [ 150, 150 ] ); ?>
								<?php else: ?>
									<img src="https://via.placeholder.com/150x150.png?text=No+Image" alt="No Image">
								<?php endif; ?>
								<div class="description">
									<h3><?php the_title(); ?></h3>
									<p
										class="address"><?php echo( ! empty( $nearGeneral['address'] ) ? $nearGeneral['address'] : '' ); ?></p>
									<div class="rating dfr">
										<ul class="dfr"
										    data-rating="<?php echo round( $commentHelpers->getStarRating( $nearID ), 0, PHP_ROUND_HALF_DOWN ) ?>">
											<li class="icon-star"></li>
											<li class="icon-star"></li>
											<li class="icon-star"></li>
											<li class="icon-star"></li>
											<li class="icon-star"></li>
										</ul>
										<p>(<?php echo $commentHelpers->getCountComment( $nearID ) ?> reviews)</p>
									</div>
									<?php $open = $nearBusinessTime->openCurrentDay(); ?>
									<p class="status icon-clock"><?php _e( 'Open until', 'jwa_locator' ); ?>
										<span>: <?php echo( ! $open ? 'Close now' : $open ) ?></span></p>
								</div>
							</a>
						</div>
					<?php endwhile;
					wp_reset_postdata();
				endif; ?>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>

