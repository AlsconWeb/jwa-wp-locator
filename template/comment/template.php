<?php
/**
 * Created 19.02.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */

$commnetClass = new \JWA_Locator\CommentForm\jwaCommentForm();
?>
<div class="review">
	<?php $args = [
		'avatar_size' => 64,
		'reply_text'  => 'Reply',
		'callback'    => [ $commnetClass, 'commentWalker' ],
	];
	?>
	<ul class="comments-list"><?php wp_list_comments( $args ); ?></ul>
	<div id="comment-nav-above">
		<?php paginate_comments_links() ?>
	</div>
</div>



