<?php
/**
 * Created 11.01.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */

namespace JWA_Locator\CPT;

use JWA_Locator\MetaBox\jwaMetaBoxLocation;

if ( ! defined( 'ABSPATH' ) ) {
	die ( 'Silly human what are you doing here' );
}


class jwaCTP {
	private $cptName;
	private $menuName;
	private $archiveSlug;
	
	public function __construct ( string $postTypeName, string $menuName, string $archiveSlug ) {
		
		$this->cptName     = $postTypeName;
		$this->menuName    = $menuName;
		$this->archiveSlug = $archiveSlug;
		
		self::registerCPT();
		self::registerTaxonomy();
		
		$metaBox = new jwaMetaBoxLocation( $this->cptName );
		
		add_filter( 'single_template', [ $this, 'templateSingleLocation' ] );
		add_filter( 'template_include', [ $this, 'addArchiveLocationTemplate' ] );
		add_filter( 'template_include', [ $this, 'addTaxonomyCity' ] );
	}
	
	/**
	 * Create CPT
	 */
	public function registerCPT () {
		register_post_type( $this->cptName, [
			'labels'             => [
				'name'               => __( $this->menuName, 'jwa_locator' ),
				'singular_name'      => __( $this->menuName, 'jwa_locator' ),
				'add_new'            => __( 'Add New', 'jwa_locator' ),
				'add_new_item'       => __( 'Add New', 'jwa_locator' ),
				'edit_item'          => __( 'Edit', 'jwa_locator' ),
				'new_item'           => __( 'New', 'jwa_locator' ),
				'view_item'          => __( 'View ' . $this->menuName, 'jwa_locator' ),
				'search_items'       => __( 'Search ' . $this->menuName, 'jwa_locator' ),
				'not_found'          => __( 'Not found', 'jwa_locator' ),
				'not_found_in_trash' => __( 'Not found', 'jwa_locator' ),
				'parent_item_colon'  => '',
				'menu_name'          => $this->menuName,
			],
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => true,
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => true,
			'menu_position'      => 5,
			'menu_icon'          => 'dashicons-admin-site',
			'has_archive'        => $this->archiveSlug,
			'supports'           => [ 'title', 'author', 'thumbnail', 'excerpt', 'comments', 'revision' ],
		] );
	}
	
	public function registerTaxonomy () {
		// tag
		register_taxonomy( 'tag-' . $this->archiveSlug, [ $this->cptName ], [
			'label'             => '',
			'labels'            => [
				'name'          => __( 'Tag', 'jwa_locator' ),
				'singular_name' => __( 'Tag', 'jwa_locator' ),
				'search_items'  => __( 'Search Tag', 'jwa_locator' ),
				'all_items'     => __( 'All Tag', 'jwa_locator' ),
				'view_item '    => __( 'View Tag', 'jwa_locator' ),
				'edit_item'     => __( 'Edit Tag', 'jwa_locator' ),
				'update_item'   => __( 'Update Tag', 'jwa_locator' ),
				'add_new_item'  => __( 'Add New Tag', 'jwa_locator' ),
				'new_item_name' => __( 'New Tag Name', 'jwa_locator' ),
				'menu_name'     => __( 'Tag', 'jwa_locator' ),
			],
			'description'       => '',
			'public'            => true,
			'hierarchical'      => false,
			'rewrite'           => true,
			'capabilities'      => [],
			'meta_box_cb'       => null,
			'show_admin_column' => true,
			'show_in_rest'      => null,
			'rest_base'         => null,
		] );
		
		//city
		register_taxonomy( 'area', [ $this->cptName ], [
			'label'             => '',
			'labels'            => [
				'name'          => __( 'Area', 'jwa_locator' ),
				'singular_name' => __( 'Area', 'jwa_locator' ),
				'search_items'  => __( 'Search Area', 'jwa_locator' ),
				'all_items'     => __( 'All Area', 'jwa_locator' ),
				'view_item '    => __( 'View Area', 'jwa_locator' ),
				'edit_item'     => __( 'Edit Area', 'jwa_locator' ),
				'update_item'   => __( 'Update Area', 'jwa_locator' ),
				'add_new_item'  => __( 'Add New Area', 'jwa_locator' ),
				'new_item_name' => __( 'New Area Name', 'jwa_locator' ),
				'menu_name'     => __( 'Area', 'jwa_locator' ),
			],
			'description'       => '',
			'public'            => true,
			'hierarchical'      => true,
			'rewrite'           => [ 'slug' => 'area', 'hierarchical' => true ],
			'capabilities'      => [],
			'meta_box_cb'       => null,
			'show_admin_column' => true,
			'show_in_rest'      => null,
			'rest_base'         => null,
		] );
		
		//category
		register_taxonomy( 'location-category', [ $this->cptName ], [
			'label'             => '',
			'labels'            => [
				'name'          => __( 'Category', 'jwa_locator' ),
				'singular_name' => __( 'Category', 'jwa_locator' ),
				'search_items'  => __( 'Search Category', 'jwa_locator' ),
				'all_items'     => __( 'All Category', 'jwa_locator' ),
				'view_item '    => __( 'View Category', 'jwa_locator' ),
				'edit_item'     => __( 'Edit Category', 'jwa_locator' ),
				'update_item'   => __( 'Update Category', 'jwa_locator' ),
				'add_new_item'  => __( 'Add New Category', 'jwa_locator' ),
				'new_item_name' => __( 'New Category Name', 'jwa_locator' ),
				'menu_name'     => __( 'Category', 'jwa_locator' ),
			],
			'description'       => '',
			'public'            => true,
			'hierarchical'      => true,
			'rewrite'           => true,
			'capabilities'      => [],
			'meta_box_cb'       => null,
			'show_admin_column' => true,
			'show_in_rest'      => null,
			'rest_base'         => null,
		] );
	}
	
	/**
	 * Checks if the theme has a custom template for output Singe Car
	 *
	 * @param $single
	 *
	 * @return string
	 */
	public function templateSingleLocation ( $single ) {
		global $post;
		
		if ( $post->post_type == $this->cptName ) {
			if ( file_exists( get_template_directory() . '/single-locator.php' ) ) {
				return $single;
			} else {
				return JWA_LOCATION_PLUGIN_DIR . '/template/single_location/single-location.php';
			}
		}
		
		return $single;
	}
	
	/**
	 * Checks if the theme has a custom template for output Archive Location
	 *
	 * @param $template
	 *
	 * @return string
	 */
	public function addArchiveLocationTemplate ( $template ) {
		if ( is_post_type_archive( JWA_LOCATION_POST_TYPE ) ) {
			$theme_files     = [ 'archive-' . JWA_LOCATION_POST_TYPE . '.php' ];
			$exists_in_theme = locate_template( $theme_files, false );
			if ( $exists_in_theme != '' ) {
				return $exists_in_theme;
			} else {
				return JWA_LOCATION_PLUGIN_DIR . '/template/archive/archive-location.php';
			}
		}
		
		return $template;
	}
	
	/**
	 * Revire
	 *
	 * @param $template
	 *
	 * @return string
	 */
	public function addTaxonomyCity ( $template ) {
		
		if ( is_tax( 'area' ) ) {
			$theme_files     = [ 'taxonomy-area.php' ];
			$exists_in_theme = locate_template( $theme_files, false );
			if ( $exists_in_theme != '' ) {
				return $exists_in_theme;
			} else {
				return JWA_LOCATION_PLUGIN_DIR . '/template/taxonomy/area/taxonomy-area.php';
			}
		}
		
		return $template;
	}
}
