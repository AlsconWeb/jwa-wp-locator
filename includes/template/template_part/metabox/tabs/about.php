<?php
/**
 * Created 17.01.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */
$helper    = new \JWA_Locator\Helpers\jwaPostData();
$mataField = $helper->getMetaField( [ 'about' ], get_the_ID() );
?>

<div class="container">
	<div class="row">
		<div class="col-12">
			<?php
			$helper->getWysiwyg( get_the_ID() );
			?>
		</div>
	</div>
</div>
