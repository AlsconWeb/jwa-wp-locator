<?php
/**
 * Created 17.01.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */

$helper    = new \JWA_Locator\Helpers\jwaPostData();
$mataField = $helper->getMetaField( [ 'phone', 'web_site', 'open_hours', 'soc' ], get_the_ID() );
?>
<div class="container">
	
	<div class="row" id="phone">
		<div class="col-6">
			<div class="mb-3">
				<label for="jwa_location_phone" class="form-label"><?php _e( 'Phone', 'jwa_locator' ); ?></label>
				<input type="tel" class="form-control" id="jwa_location_phone" name="jwa_location_phone"
				       value="<?php echo( ! empty( $mataField['phone'] ) && isset( $mataField['phone'] ) ? $mataField['phone'] : '' ) ?>"
				>
				<p class="description"><?php _e( 'Format: 123-456-6789', 'jwa_locator' ); ?></p>
			</div>
		</div>
		<div class="col-6">
			<div class="mb-3">
				<label for="jwa_location_web_site" class="form-label"><?php _e( 'WebSite', 'jwa_locator' ); ?></label>
				<input type="text" class="form-control" id="jwa_location_web_site" name="jwa_location_web_site"
				       placeholder="http://exemple.com"
				       value="<?php echo( ! empty( $mataField['web_site'] ) && isset( $mataField['web_site'] ) ? $mataField['web_site'] : '' ) ?>"
				>
			</div>
		</div>
	</div>
	
	<div class="row" id="time">
		<div class="col-12">
			<h4><?php _e( 'Business Hours', 'jwa_locator' ); ?></h4>
		</div>
		<div class="col-6">
			<h5><?php _e( 'Open', 'jwa_locator' ); ?></h5>
			<div class="mb-3">
				<label for="jwa_location_open_hours_mon" class="form-label"><?php _e( 'Mon.', 'jwa_locator' ); ?></label>
				<input type="text" name="jwa_location_open_hours[mon][open]" id="jwa_location_open_hours_mon"
				       style="width: 100%"
				       value="<?php echo( ! empty( $mataField['open_hours']['mon']['open'] ) && isset( $mataField['open_hours']['mon']['open'] ) ? $mataField['open_hours']['mon']['open'] : '' ) ?>">
			</div>
			
			<div class="mb-3">
				<label for="jwa_location_open_hours_tues" class="form-label"><?php _e( 'Tues.', 'jwa_locator' ); ?></label>
				<input type="text" name="jwa_location_open_hours[tue][open]" id="jwa_location_open_hours_tues"
				       style="width: 100%"
				       value="<?php echo( ! empty( $mataField['open_hours']['tue']['open'] ) && isset( $mataField['open_hours']['tue']['open'] ) ? $mataField['open_hours']['tue']['open'] : '' ) ?>">
			</div>
			
			<div class="mb-3">
				<label for="jwa_location_open_hours_wed" class="form-label"><?php _e( 'Wed.', 'jwa_locator' ); ?></label>
				<input type="text" name="jwa_location_open_hours[wed][open]" id="jwa_location_open_hours_wed"
				       style="width: 100%"
				       value="<?php echo( ! empty( $mataField['open_hours']['wed']['open'] ) && isset( $mataField['open_hours']['wed']['open'] ) ? $mataField['open_hours']['wed']['open'] : '' ) ?>">
			</div>
			
			<div class="mb-3">
				<label for="jwa_location_open_hours_thurs" class="form-label"><?php _e( 'Thurs.', 'jwa_locator' ); ?></label>
				<input type="text" name="jwa_location_open_hours[thu][open]" id="jwa_location_open_hours_thu"
				       style="width: 100%"
				       value="<?php echo( ! empty( $mataField['open_hours']['thu']['open'] ) && isset( $mataField['open_hours']['thu']['open'] ) ? $mataField['open_hours']['thu']['open'] : '' ) ?>">
			</div>
			
			<div class="mb-3">
				<label for="jwa_location_open_hours_fri" class="form-label"><?php _e( 'Fri.', 'jwa_locator' ); ?></label>
				<input type="text" name="jwa_location_open_hours[fri][open]" id="jwa_location_open_hours_fri"
				       style="width: 100%"
				       value="<?php echo( ! empty( $mataField['open_hours']['fri']['open'] ) && isset( $mataField['open_hours']['fri']['open'] ) ? $mataField['open_hours']['fri']['open'] : '' ) ?>">
			</div>
			
			<div class="mb-3">
				<label for="jwa_location_open_hours_sat" class="form-label"><?php _e( 'Sat.', 'jwa_locator' ); ?></label>
				<input type="text" name="jwa_location_open_hours[sat][open]" id="jwa_location_open_hours_sat"
				       style="width: 100%"
				       value="<?php echo( ! empty( $mataField['open_hours']['sat']['open'] ) && isset( $mataField['open_hours']['sat']['open'] ) ? $mataField['open_hours']['sat']['open'] : '' ) ?>">
			</div>
			
			<div class="mb-3">
				<label for="jwa_location_open_hours_sun" class="form-label"><?php _e( 'Sun.', 'jwa_locator' ); ?></label>
				<input type="text" name="jwa_location_open_hours[sun][open]" id="jwa_location_open_hours_sun"
				       style="width: 100%"
				       value="<?php echo( ! empty( $mataField['open_hours']['sun']['open'] ) && isset( $mataField['open_hours']['sun']['open'] ) ? $mataField['open_hours']['sun']['open'] : '' ) ?>">
			</div>
		
		</div>
		<div class="col-6">
			<h5><?php _e( 'Close', 'jwa_locator' ); ?></h5>
			<div class="mb-3">
				<label for="jwa_location_close_hours_mon" class="form-label"><?php _e( 'Mon.', 'jwa_locator' ); ?></label>
				<input type="text" name="jwa_location_open_hours[mon][close]" id="jwa_location_close_hours_mon"
				       style="width: 100%"
				       value="<?php echo( ! empty( $mataField['open_hours']['mon']['close'] ) && isset( $mataField['open_hours']['mon']['close'] ) ? $mataField['open_hours']['mon']['close'] : '' ) ?>">
			</div>
			
			<div class="mb-3">
				<label for="jwa_location_close_hours_tues" class="form-label"><?php _e( 'Tues.', 'jwa_locator' ); ?></label>
				<input type="text" name="jwa_location_open_hours[tue][close]" id="jwa_location_close_hours_tues"
				       style="width: 100%"
				       value="<?php echo( ! empty( $mataField['open_hours']['tue']['close'] ) && isset( $mataField['open_hours']['tue']['close'] ) ? $mataField['open_hours']['tue']['close'] : '' ) ?>">
			</div>
			
			<div class="mb-3">
				<label for="jwa_location_close_hours_wed" class="form-label"><?php _e( 'Wed.', 'jwa_locator' ); ?></label>
				<input type="text" name="jwa_location_open_hours[wed][close]" id="jwa_location_close_hours_wed"
				       style="width: 100%"
				       value="<?php echo( ! empty( $mataField['open_hours']['wed']['close'] ) && isset( $mataField['open_hours']['wed']['close'] ) ? $mataField['open_hours']['wed']['close'] : '' ) ?>">
			</div>
			
			<div class="mb-3">
				<label for="jwa_location_close_hours_thu" class="form-label"><?php _e( 'Thurs.', 'jwa_locator' ); ?></label>
				<input type="text" name="jwa_location_open_hours[thu][close]" id="jwa_location_close_hours_thu"
				       style="width: 100%"
				       value="<?php echo( ! empty( $mataField['open_hours']['thu']['close'] ) && isset( $mataField['open_hours']['thu']['close'] ) ? $mataField['open_hours']['thu']['close'] : '' ) ?>">
			</div>
			
			<div class="mb-3">
				<label for="jwa_location_close_hours_fri" class="form-label"><?php _e( 'Fri.', 'jwa_locator' ); ?></label>
				<input type="text" name="jwa_location_open_hours[fri][close]" id="jwa_location_close_hours_fri"
				       style="width: 100%"
				       value="<?php echo( ! empty( $mataField['open_hours']['fri']['close'] ) && isset( $mataField['open_hours']['fri']['close'] ) ? $mataField['open_hours']['fri']['close'] : '' ) ?>">
			</div>
			
			<div class="mb-3">
				<label for="jwa_location_close_hours_sat" class="form-label"><?php _e( 'Sat.', 'jwa_locator' ); ?></label>
				<input type="text" name="jwa_location_open_hours[sat][close]" id="jwa_location_close_hours_sat"
				       style="width: 100%"
				       value="<?php echo( ! empty( $mataField['open_hours']['sat']['close'] ) && isset( $mataField['open_hours']['sat']['close'] ) ? $mataField['open_hours']['sat']['close'] : '' ) ?>">
			</div>
			
			<div class="mb-3">
				<label for="jwa_location_close_hours_sun" class="form-label"><?php _e( 'Sun.', 'jwa_locator' ); ?></label>
				<input type="text" name="jwa_location_open_hours[sun][close]" id="jwa_location_close_hours_sun"
				       style="width: 100%"
				       value="<?php echo( ! empty( $mataField['open_hours']['sun']['close'] ) && isset( $mataField['open_hours']['sun']['close'] ) ? $mataField['open_hours']['sun']['close'] : '' ) ?>">
			</div>
		</div>
	</div>
	
	<div class="row" id="soc">
		<div class="col-12">
			<h4><?php _e( 'Social Network ', 'jwa_locator' ); ?></h4>
		</div>
		<div class="col-12">
			<div class="mb-3">
				<label for="jwa_location_soc_fb" class="form-label"><?php _e( 'Facebook', 'jwa_locator' ); ?></label>
				<input type="text" class="form-control" id="jwa_location_soc_fb" name="jwa_location_soc[fb]"
				       value="<?php echo( ! empty( $mataField['soc']['fb'] ) && isset( $mataField['soc']['fb'] ) ? $mataField['soc']['fb'] : '' ) ?>">
			</div>
		</div>
		<div class="col-12">
			<div class="mb-3">
				<label for="jwa_location_soc_tw" class="form-label"><?php _e( 'Twitter', 'jwa_locator' ); ?></label>
				<input type="text" class="form-control" id="jwa_location_soc_tw" name="jwa_location_soc[tw]"
				       value="<?php echo( ! empty( $mataField['soc']['tw'] ) && isset( $mataField['soc']['tw'] ) ? $mataField['soc']['tw'] : '' ) ?>">
			</div>
		</div>
		<div class="col-12">
			<div class="mb-3">
				<label for="jwa_location_soc_in" class="form-label"><?php _e( 'Linkedin', 'jwa_locator' ); ?></label>
				<input type="text" class="form-control" id="jwa_location_soc_in" name="jwa_location_soc[in]"
				       value="<?php echo( ! empty( $mataField['soc']['in'] ) && isset( $mataField['soc']['in'] ) ? $mataField['soc']['in'] : '' ) ?>">
			</div>
		</div>
		<div class="col-12"></div>
	</div>


</div>
