<?php
/**
 * Created 17.01.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */

/**
 * @TODO add rating starts
 */
$id        = get_the_ID();
$helper    = new \JWA_Locator\Helpers\jwaPostData();
$mataField = $helper->getMetaField( [ 'reviews' ], $id );
if ( ! empty( $mataField[0] ) ):
	foreach ( $mataField['reviews'] as $key => $review ):
		?>
		<div class="card" id="review-<?php echo $key ?>" data-id="<?php echo $key ?>" data-post_id="<?php echo $id ?>">
			<div class="card-body">
				<div class="form-check">
					<input class="form-check-input" type="checkbox"
					       value="<?php echo( $review['status'] == 'publish' ? 'publish' : 'draft' ) ?>"
					       id="jwa_review_status-<?php echo $key ?>"
						<?php echo( $review['status'] == 'publish' ? 'checked' : '' ); ?>
					>
					<label class="form-check-label" for="jwa_review_status-<?php echo $key ?>">
						<?php _e( 'Status', 'jwa_locator' ); ?>
					</label>
				</div>
				<h5 class="card-title"><?php echo $review['reviewer'] ?></h5>
				<h6 class="card-subtitle mb-2 text-muted"><?php echo $review['title'] ?></h6>
				<p class="card-text"><?php echo $review['text'] ?></p>
				<a href="<?php the_permalink(); ?>" class="card-link"><?php _e( 'Location link', 'jwa_locator' ); ?></a>
				<a href="#" class="card-link delete-review"><?php _e( 'Delete Review', 'jwa_locator' ); ?></a>
			</div>
		</div>
	<?php endforeach; endif; ?>