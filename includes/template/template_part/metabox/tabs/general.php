<?php
/**
 * Created 17.01.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */

$helper    = new \JWA_Locator\Helpers\jwaPostData();
$mataField = $helper->getInLocationTable( get_the_ID() );
?>
<div class="container">
	<div class="row">
		<div class="col-12">
			<div class="mb-3">
				<label for="jwa_location_address" class="form-label"><?php _e( 'Address', 'jwa_locator' ); ?></label>
				<input type="text" class="form-control" id="jwa_location_address" name="jwa_location_address"
				       value="<?php echo( ! empty( $mataField['address'] ) && isset( $mataField['address'] ) ? $mataField['address'] : '' ) ?>">
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-4">
			<div class="mb-3">
				<label for="jwa_location_postal_code" class="form-label"><?php _e( 'Postal Code', 'jwa_locator' ); ?></label>
				<input type="text" class="form-control" id="jwa_location_postal_code" name="jwa_location_postal_code"
				       value="<?php echo( ! empty( $mataField['postal_code'] ) && isset( $mataField['postal_code'] ) ?
					       $mataField['postal_code'] : '' ) ?>">
			</div>
		</div>
		<div class="col-4">
			<div class="mb-3">
				<label for="jwa_location_country" class="form-label"><?php _e( 'Country', 'jwa_locator' ); ?></label>
				<select class="form-select" id="jwa_location_country" name="jwa_location_country" style="width: 100%"
				        aria-label="<?php _e( 'Select a country', 'jwa_locator' ); ?>">
					<option selected value="0"><?php _e( 'Select a country', 'jwa_locator' ); ?></option>
					<option value="USA" <?php echo( ! empty( $mataField['country'] ) && isset( $mataField['country'] ) &&
					                                $mataField['country'] == 'USA' ? 'selected' : '' ) ?>><?php _e( 'USA', 'jwa_locator' ); ?></option>
					<option value="CA" <?php echo( ! empty( $mataField['country'] ) && isset( $mataField['country'] ) &&
					                               $mataField['country'] == 'CA' ? 'selected' : '' ) ?>><?php _e( 'Canada', 'jwa_locator' ); ?></option>
				</select>
			</div>
		</div>
		<div class="col-4">
			<div class="mb-3">
				<label for="jwa_location_province" class="form-label"><?php _e( 'Province', 'jwa_locator' ); ?></label>
				<select class="form-select" id="jwa_location_province" name="jwa_location_province" style="width: 100%"
				        aria-label="<?php _e( 'Select a province', 'jwa_locator' ); ?>">
					<option selected value="0"><?php _e( 'Select a province', 'jwa_locator' ); ?></option>
					<?php foreach ( $helper->getCanadaProvince() as $key => $province ): ?>
						<option
							value="<?php echo $key; ?>"
							<?php echo( ! empty( $mataField['province'] ) && isset( $mataField['province'] ) &&
							            $mataField['province'] == $key ? 'selected' : '' ) ?>><?php echo $province; ?></option>
					<?php endforeach; ?>
				</select>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-12">
			<div class="md-3">
				<div id="map" style="height: 450px;"></div>
			</div>
		</div>
	</div>
	<input type="hidden" name="jwa_location_city" id="jwa_location_city" value="">
	<input type="hidden" name="jwa_location_lat" id="jwa_location_lat"
	       value="<?php echo( ! empty( $mataField['lat'] ) && isset( $mataField['lat'] ) ? $mataField['lat'] : '' ) ?>">
	<input type="hidden" name="jwa_location_lng" id="jwa_location_lng"
	       value="<?php echo( ! empty( $mataField['lng'] ) && isset( $mataField['lng'] ) ? $mataField['lng'] : '' ) ?>">
</div>

