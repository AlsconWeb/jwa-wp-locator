<?php
/**
 * Created 17.01.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */

$helper = new \JWA_Locator\Helpers\jwaPostData();
$id     = get_the_ID();
$images = $helper->getMetaField( [ 'gallery' ], $id );
?>
<div class="card-group">
	<?php if ( ! empty( $images['gallery'] ) ):
		if ( is_string( $images['gallery'] ) ) {
			$imagesID = explode( ',', $images['gallery'] );
		} else {
			$imagesID = $images['gallery'];
		}
		?>
		<?php foreach ( $imagesID as $key => $image ): ?>
		
		<div class="mb-3 image-gallery-metabox">
			<a href="#" class="btn btn-secondary delete-image" data-index="<?php echo $key ?>">
				<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-square"
				     viewBox="0 0 16 16">
					<path
						d="M14 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h12zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/>
					<path
						d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
				</svg>
			</a>
			<img src="<?php echo wp_get_attachment_image_url( $image, 'full' ); ?>" class="rounded img-thumbnail mb-3 mx-auto
					d-block"
			     alt="<?php echo get_the_title( $image ) ?>">
		</div>
	<?php endforeach; ?>
	<?php endif; ?>
</div>
<div class="container">
	<div class="row">
		<div class="col-12">
			<input type="hidden" name="jwa_location_gallery" id="jwa_location_gallery"
			       value="<?php echo( is_string( $images['gallery'] ) ? $images['gallery'] : implode( ',', $images['gallery'] ) ) ?>">
			<input type="hidden" name="jwa_location_post_id" id="post_id" value="<?php echo $id; ?>">
			<div class="mt-3 mb-3">
				<a class="btn btn-primary" id="jwa_upload" href="#"><?php _e( 'Upload images', 'jwa_locator' ); ?></a>
				<a class="btn btn-danger" id="jwa_remove_all" href="#"><?php _e( 'Remove all images', 'jwa_locator' ); ?></a>
			</div>
		</div>
	</div>
</div>
