<?php
/**
 * Created 17.03.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */

?>

<h1><?php _e( 'Settings Location', 'jwa_locator' ); ?></h1>
<form method="POST" action="options.php">
	<?php
	settings_fields( 'jwa_location_group' );
	do_settings_sections( 'location-settings' );
	?>
	<div class="form-control">
		<input type="submit" name="submit" value="<?php _e( 'Save Changes', 'jwa_locator' ); ?>"
		       class="button button-primary">
	</div>
</form>
