<?php
/**
 * Created 01.03.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */

if ( isset( $term ) ) {
	$lat  = get_term_meta( $term->term_id, 'jwa_location_lat', true );
	$lng  = get_term_meta( $term->term_id, 'jwa_location_lng', true );
	$city = get_term_meta( $term->term_id, 'jwa_location_city', true );
}
?>
<tr class="form-field">
	<th scope="row" valign="top"><label for="jwa_location_city"><?php _e( 'City in map', 'jwa_locator' ); ?></label></th>
	<td>
		<input type="text" name="jwa_location_city" id="jwa_location_city" size="40"
		       value="<?php echo( isset( $city ) ? $city : '' ); ?>"/>
		<p><?php _e( 'Choice city in map' ); ?></p>
		<input type="hidden" name="jwa_location_lat" id="jwa-location-lat"
		       value="<?php echo( isset( $lat ) ? $lat : '' ); ?>">
		<input type="hidden" name="jwa_location_lng" id="jwa-location-lng" value="<?php echo( isset( $lng ) ? $lng : '' );
		?>">
		<div id="map-in-city" style="width: 100%; height: 400px;"></div>
	</td>
</tr>

