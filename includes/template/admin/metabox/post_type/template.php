<?php
/**
 * Created 16.01.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */

?>
<!--start nav-->
<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
	<!--start general nav-->
	<li class="nav-item" role="presentation">
		<a class="nav-link active" id="general-tab" data-bs-toggle="pill" href="#general" role="tab"
		   aria-controls="pills-home" aria-selected="true"><?php _e( 'General', 'jwa_locator' ); ?></a>
	</li>
	<!--end general nav-->
	
	<!--start contact-info nav-->
	<li class="nav-item" role="presentation">
		<a class="nav-link" id="contact-info-tab" data-bs-toggle="pill" href="#contact-info" role="tab"
		   aria-controls="pills-profile" aria-selected="false"><?php _e( 'Contact Info', 'jwa_locator' ); ?></a>
	</li>
	<!--end contact-info nav-->
	
	<!--start about nav-->
	<li class="nav-item" role="presentation">
		<a class="nav-link" id="about-tab" data-bs-toggle="pill" href="#about" role="tab"
		   aria-controls="pills-contact" aria-selected="false"><?php _e( 'About', 'jwa_locator' ); ?></a>
	</li>
	<!--end about nav-->
	
	<!--start gallery nav-->
	<li class="nav-item" role="presentation">
		<a class="nav-link" id="gallery-tab" data-bs-toggle="pill" href="#gallery" role="tab"
		   aria-controls="pills-contact" aria-selected="false"><?php _e( 'Gallery', 'jwa_locator' ); ?></a>
	</li>
	<!--end gallery nav-->
	
	<!--start review nav-->
	<li class="nav-item" role="presentation">
		<a class="nav-link" id="review-tab" data-bs-toggle="pill" href="#review" role="tab"
		   aria-controls="pills-contact" aria-selected="false"><?php _e( 'Review', 'jwa_locator' ); ?></a>
	</li>
	<!--end review nav-->

</ul>
<!--end nav-->

<!--start tabs-->
<div class="tab-content" id="pills-tabContent">
	<!--start general tab-->
	<div class="tab-pane fade show active" id="general" role="tabpanel"
	     aria-labelledby="general-tab">
	  <?php include_once JWA_LOCATION_PLUGIN_DIR . '/includes/template/template_part/metabox/tabs/general.php'; ?>
	</div>
	<!--end general tab-->
	
	<!--start contact-info tab-->
	<div class="tab-pane fade" id="contact-info" role="tabpanel" aria-labelledby="contact-info-tab">
	  <?php include_once JWA_LOCATION_PLUGIN_DIR . '/includes/template/template_part/metabox/tabs/contact-info.php'; ?>
	</div>
	<!--end contact-info tab-->
	
	<!--start about tab-->
	<div class="tab-pane fade" id="about" role="tabpanel" aria-labelledby="about-tab">
	  <?php include_once JWA_LOCATION_PLUGIN_DIR . '/includes/template/template_part/metabox/tabs/about.php'; ?>
	</div>
	<!--end about tab-->
	
	<!--start gallery tab-->
	<div class="tab-pane fade" id="gallery" role="tabpanel" aria-labelledby="gallery-tab">
	  <?php include_once JWA_LOCATION_PLUGIN_DIR . '/includes/template/template_part/metabox/tabs/gallery.php'; ?>
	</div>
	<!--end gallery tab-->
	
	<!--start review tab-->
	<div class="tab-pane fade" id="review" role="tabpanel" aria-labelledby="review-tab">
	  <?php include_once JWA_LOCATION_PLUGIN_DIR . '/includes/template/template_part/metabox/tabs/review.php'; ?>
	</div>
	<!--end review tab-->
</div>
<!--end tabs-->

