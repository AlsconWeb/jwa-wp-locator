<?php
/**
 * Created 01.03.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */

namespace JWA_Locator\metaField;

use JWA_Locator\Helpers\jwaPostData;

if ( ! defined( 'ABSPATH' ) ) {
	die ( 'Silly human what are you doing here' );
}

class jwaCityField {
	private $helpers;
	
	public function __construct () {
		$this->helpers = new jwaPostData();
		
		// add field
		add_action( 'area_add_form_fields', [ $this, 'addMetaField' ], 10, 1 );
		add_action( 'area_edit_form_fields', [ $this, 'editMetaField' ], 10, 2 );
		
		// save value
		add_action( 'created_area', [ $this, 'saveTermMeta' ] );
		add_action( 'edited_area', [ $this, 'saveTermMeta' ] );
		
		add_action( 'admin_enqueue_scripts', [ $this, 'addScriptsAdmin' ] );
	}
	
	/**
	 * Add Meta Field to Add City
	 *
	 * @param $taxonomy
	 */
	public function addMetaField ( $taxonomy ) {
		include_once JWA_LOCATION_PLUGIN_DIR . '/includes/template/admin/metaField/city/template.php';
	}
	
	/**
	 * Add Meta Field to Edit City
	 *
	 * @param $term
	 * @param $taxonomy
	 */
	public function editMetaField ( $term, $taxonomy ) {
		include_once JWA_LOCATION_PLUGIN_DIR . '/includes/template/admin/metaField/city/template.php';
	}
	
	
	public function saveTermMeta ( $term_id ) {
		update_term_meta(
			$term_id,
			'jwa_location_city',
			sanitize_text_field( $_POST['jwa_location_city'] )
		);
		
		update_term_meta(
			$term_id,
			'jwa_location_lat',
			sanitize_text_field( $_POST['jwa_location_lat'] )
		);
		
		update_term_meta(
			$term_id,
			'jwa_location_lng',
			sanitize_text_field( $_POST['jwa_location_lng'] )
		);
	}
	
	/**
	 * Add Script to page edit-tags.php, term.php in CPT Location
	 *
	 * @param $hook_suffix
	 */
	public function addScriptsAdmin ( $hook_suffix ) {
		
		if ( $hook_suffix == 'term.php' || $hook_suffix == 'edit-tags.php' && $_GET['post_type'] == JWA_LOCATION_POST_TYPE ) {
			wp_enqueue_script( 'g-map', "//maps.googleapis.com/maps/api/js?key=" . JWA_LOCATION_GOOGLE_KEY . '&libraries=places&language=en', [ 'jquery' ],
				'1.0.0',
				true );
			wp_enqueue_script( 'map-taxanomy', JWA_LOCATION_PLUGIN_URL . '/assets/js/admin/metaField/map.js', [ 'jquery' ],
				$this->helpers->versionFile( '/assets/js/admin//metaField/map.js' ),
				true );
		}
	}
	
}