<?php
/**
 * Created 25.01.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */

namespace JWA_Locator\Rest;

use JWA_Locator\Helpers\jwaLocationReview;
use JWA_Locator\Helpers\jwaPostData;

if ( ! defined( 'ABSPATH' ) ) {
	die ( 'Silly human what are you doing here' );
}

class jwaLocationRESTApi {
	private $helper;
	
	
	public function __construct () {
		$this->helper = new jwaPostData();
		add_action( 'rest_api_init', [ $this, 'addRegisterRest' ] );
	}
	
	public function addRegisterRest () {
		register_rest_route( JWA_REST_NAMESPACE, '/location/(?P<id>[\d]+)', [
			[
				'methods'  => 'GET',
				'callback' => [ $this, 'getLocationByID' ],
			],
			[
				'methods'  => 'POST',
				'callback' => [ $this, 'updateLocationByID' ],
				'args'     => [
					'token' => [ 'type' => 'string', 'required' => false ],
					'id'    => [ 'type' => 'integer', 'required' => true ],
				],
			],
		] );
		
		register_rest_route( JWA_REST_NAMESPACE, '/location/', [
			[
				'methods'             => 'POST',
				'callback'            => [ $this, 'insertLocation' ],
				'args'                => [
					'token'      => [ 'type' => 'string', 'required' => false ],
					'title'      => [ 'type' => 'string', 'required' => false ],
					'status'     => [ 'type' => 'string', 'required' => false ],
					'address'    => [ 'type' => 'string', 'required' => true ],
					'city'       => [ 'type' => 'array', 'required' => false ],
					'country'    => [ 'type' => 'string', 'required' => false ],
					'province'   => [ 'type' => 'array', 'required' => false ],
					'lat'        => [ 'type' => 'string', 'required' => true ],
					'lng'        => [ 'type' => 'string', 'required' => true ],
					'phone'      => [ 'type' => 'string', 'required' => false ],
					'web_site'   => [ 'type' => 'string', 'required' => false ],
					'open_hours' => [ 'type' => 'array', 'required' => false ],
					// ex. ['sun'=>['open']=>'1:30','sun'=>['close']=>'12:00']
					'soc'        => [ 'type' => 'array', 'required' => false ],
					// ex. ['fb'=>'url', 'tw'=>'url', 'in'=>'url']
					'about'      => [ 'type' => 'string', 'required' => false ],
					'logo'       => [ 'type' => 'string', 'required' => false ],
					'gallery'    => [ 'type' => 'array', 'required' => false ],
					//ex. ['url-image','url-image','url-image']
					'category'   => [ 'type' => 'array', 'required' => false ],
					//ex. ['category 1', 'category 2' ...]
					'tag'        => [ 'type' => 'array', 'required' => false ],
					//ex. ['tag 1', 'tag 1' ...]
				],
				'permission_callback' => [ $this, 'tokenChecks' ],
			],
		] );
		
		register_rest_route( JWA_REST_NAMESPACE, '/location/review/(?P<post_id>[\d]+)', [
			[
				'methods'  => 'GET',
				'callback' => [ $this, 'getLocationReviews' ],
			],
			[
				'methods'  => 'POST',
				'callback' => [ $this, 'addLocationReviews' ],
				'args'     => [
					'token'   => [ 'type' => 'string', 'required' => false ],
					'reviews' => [ 'type' => 'array', 'required' => true ],
				],
			],
		] );
	}
	
	/**
	 * Get location Info
	 *
	 * @param \WP_REST_Request $request
	 *
	 * @return array|\WP_Error
	 */
	public function getLocationByID ( \WP_REST_Request $request ) {
		$params = $request->get_params();
		
		if ( empty( $params['id'] ) ) {
			return new \WP_Error( '404', 'Location ID not transferred' );
		}
		
		$post = get_posts( [ 'post_type' => JWA_LOCATION_POST_TYPE, 'p' => (int) $params['id'] ] );
		$meta = $this->helper->getMetaField( [
			'address',
			'city',
			'country',
			'province',
			'lat',
			'lng',
			'phone',
			'web_site',
			'open_hours',
			'soc',
			'about',
			'gallery',
			'reviews',
			'category',
			'tag',
		],
			$params['id'] );
		
		
		$getInfo                 = [
			'title'      => $post[0]->post_title,
			'status'     => $post[0]->post_status,
			'gallery'    => $this->helper->getGalleryLocation( $params['id'] ),
			'category'   => $meta['category'],
			'tag'        => $meta['tag'],
			'phone'      => $meta['phone'],
			'web_site'   => $meta['web_site'],
			'open_hours' => $meta['open_hours'],
			'soc'        => $meta['soc'],
			'about'      => $meta['about'],
		];
		$getInfo['general_info'] = $this->helper->getInLocationTable( $params['id'] );
		
		return $getInfo;
	}
	
	/**
	 * Create new Location
	 *
	 * @param \WP_REST_Request $request
	 *
	 * @return array|\WP_Error
	 */
	public function insertLocation ( \WP_REST_Request $request ) {
		$params = $request->get_params();
		
		if ( ! self::tokenChecks( $request ) ) {
			return [ 'code' => 403, 'message' => __( 'Access Denied check the validity of the the token', 'jwa_locator' ) ];
		}
		
		$metaArray = $this->helper->setMetaField( $params );
		
		$postData = [
			'post_title'   => $params['title'],
			'post_status'  => isset( $params['status'] ) ? $params['status'] : 'publish',
			'post_author'  => 1,
			'post_type'    => JWA_LOCATION_POST_TYPE,
			'post_name'    => '',
			'post_content' => '',
			'meta_input'   => $metaArray,
		];
		
		$postID = wp_insert_post( $postData, true );
		
		if ( is_wp_error( $postID ) ) {
			return [ 'statusCode' => 500, 'error' => $postID->get_error_message() ];
		}
		
		$locationData = [];
		foreach ( $params as $key => $value ) {
			switch ( $key ) {
				case 'address':
					$locationData['address'] = $value;
					break;
				case 'postal_code':
					$locationData['postal_code'] = $value;
					break;
				case 'country':
					$locationData['country']['name'] = $value;
					break;
				case 'province':
					$locationData['province']['name'] = $value;
					break;
				case 'lat':
					$locationData['lat'] = $value;
					break;
				case 'lng':
					$locationData['lng'] = $value;
					break;
			}
		}
		
		$resp = $this->helper->insertToLocationTable( $postID, $locationData );
		
		if ( isset( $params['logo'] ) && ! empty( $params['logo'] ) ) {
			$logo = $this->helper->uploadImagGallery( [ $params['logo'] ] );
			$this->helper->setThumbnail( $logo, $postID );
		} else {
			$this->helper->setThumbnail( $metaArray['jwa_location_gallery'], $postID );
		}
		
		$category = $this->helper->setTerm( $postID, $params['category'], 'location-category' );
		$tag      = $this->helper->setTerm( $postID, $params['tag'], 'tag-' . JWA_LOCATION_ARCHIVE_SLUG );
		$province = $this->helper->setTerm( $postID, $params['province'][0]['name'], 'area' );
		$city     = $this->helper->setTerm( $postID, $params['city'][0]['name'], 'area', $province[0] );
		
		if ( ! empty( $params['city'][0]['lat'] ) ) {
			$this->helper->setTermMeta( (int) $city[0], 'jwa_location_lat', (float) $params['city'][0]['lat'] );
			$this->helper->setTermMeta( (int) $city[0], 'jwa_location_lng', (float) $params['city'][0]['lng'] );
		}
		
		if ( ! empty( $params['province'][0]['lat'] ) ) {
			$res_latP = $this->helper->setTermMeta( (int) $province[0], 'jwa_location_lat', (float) $params['province'][0]['lat'] );
			$res_lngP = $this->helper->setTermMeta( (int) $province[0], 'jwa_location_lng',
				(float) $params['province'][0]['lng'] );
		}
		
		
		return [
			'statusCode'     => 200,
			'locationID'     => $postID,
			'set_category'   => $category,
			'set_tag'        => $tag,
			'set_city'       => $city,
			'set_provice'    => $province,
			'location_table' => $resp,
			'link'           => get_the_permalink( $postID ),
		];
		
	}
	
	/**
	 * Update location
	 *
	 * @param \WP_REST_Request $request
	 *
	 * @return array|\WP_Error
	 */
	public function updateLocationByID ( \WP_REST_Request $request ) {
		$params = $request->get_params();
		
		if ( empty( $params['id'] ) ) {
			return [ 'code' => 404, 'message' => __( 'Location ID not transferred', 'jwa_locator' ) ];
		}
		
		if ( ! self::tokenChecks( $request ) ) {
			return [ 'code' => 403, 'message' => __( 'Access Denied check the validity of the the token', 'jwa_locator' ) ];
		}
		
		$arg = [
			"ID"           => $params['id'],
			"post_status"  => $params['status'],
			"post_content" => ( ! empty( $params['content'] ) ? wp_kses_allowed_html( $params['content'] ) : '' ),
			"post_title"   => $params['title'],
		];
		
		$postID = wp_update_post( wp_slash( $arg ) );
		
		if ( is_wp_error( $postID ) ) {
			return [ 'statusCode' => 500, 'message' => $postID->get_error_message() ];
		}
		
		$metaFields = $this->helper->setMetaField( $params, $params['id'] );
		if ( ! empty( $metaFields ) ) {
			
			foreach ( $metaFields as $key => $field ) {
				update_post_meta( $postID, $key, $field );
			}
			
			$locationData = [];
			foreach ( $params as $key => $value ) {
				switch ( $key ) {
					case 'address':
						$locationData['address'] = $value;
						break;
					case 'postal_code':
						$locationData['postal_code'] = $value;
						break;
					case 'country':
						$locationData['country'] = $value;
						break;
					case 'province':
						$locationData['province'] = $value;
						break;
					case 'lat':
						$locationData['lat'] = $value;
						break;
					case 'lng':
						$locationData['lng'] = $value;
						break;
				}
			}
			if ( ! empty( $locationData ) ) {
				$resp = $this->helper->insertToLocationTable( $postID, $locationData );
			}
		} else {
			return [ 'statusCode' => 500, 'message' => 'no params' ];
		}
		
		if ( isset( $params['logo'] ) && ! empty( $params['logo'] ) ) {
			$logo = $this->helper->uploadImagGallery( [ $params['logo'] ] );
			$this->helper->setThumbnail( $logo, $postID );
		} else {
			$this->helper->setThumbnail( $locationData['jwa_location_gallery'], $postID );
		}
		
		$category = $this->helper->setTerm( $params['id'], $params['category'], 'location-category' );
		$tag      = $this->helper->setTerm( $params['id'], $params['tag'], 'tag-' . JWA_LOCATION_ARCHIVE_SLUG );
		
		$province = $this->helper->setTerm( $postID, $params['province'][0]['name'], 'area' );
		$city     = $this->helper->setTerm( $postID, $params['city'][0]['name'], 'area', $province[0] );
		
		if ( ! empty( $params['city'][0]['lat'] ) ) {
			$this->helper->setTermMeta( (int) $city[0], 'jwa_location_lat', (float) $params['city'][0]['lat'] );
			$this->helper->setTermMeta( (int) $city[0], 'jwa_location_lng', (float) $params['city'][0]['lng'] );
		}
		
		if ( ! empty( $params['province'][0]['lat'] ) ) {
			$res_latP = $this->helper->setTermMeta( (int) $province[0], 'jwa_location_lat', (float) $params['province'][0]['lat'] );
			$res_lngP = $this->helper->setTermMeta( (int) $province[0], 'jwa_location_lng',
				(float) $params['province'][0]['lng'] );
		}
		
		return [
			'status'         => 200,
			'locationID'     => $postID,
			'category'       => $category,
			'set_tag'        => $tag,
			'set_city'       => $city,
			'set_provice'    => $province,
			'location_table' => $resp,
			'link'           => get_the_permalink( $params['id'], true ),
		];
	}
	
	
	/**
	 * Get Location Reviews
	 *
	 * @param \WP_REST_Request $request
	 *
	 * @return array
	 */
	public function getLocationReviews ( \WP_REST_Request $request ) {
		$params = $request->get_params();
		$post   = get_post( $params['post_id'] );
		
		if ( empty( $params['post_id'] ) || $post->post_type !== JWA_LOCATION_POST_TYPE ) {
			return [ 'code' => 404, 'message' => __( 'Location ID not transferred or not find', 'jwa_locator' ) ];
		}
		$args     = [ 'status' => 'approve', ];
		$comments = get_comments( $args );
		$answer   = [];
		if ( ! empty( $comments ) ) {
			foreach ( $comments as $comment ) {
				$commentMeta = new jwaLocationReview();
				$answer[]    = [
					'comment_id'      => $comment->comment_ID,
					'post_id'         => $comment->comment_post_ID,
					'author'          => $comment->comment_author,
					'comment_date'    => $comment->comment_date,
					'comment_content' => $comment->comment_content,
					'comment_parent'  => $comment->comment_parent,
					'avatar'          => $commentMeta->getAvatarUser( $comment->comment_ID ),
					'rating'          => $commentMeta->getRatingInComments( $comment->comment_ID ),
				];
				
			}
		}
		
		return $answer;
		
	}
	
	/**
	 * Add Location Review
	 *
	 * @param \WP_REST_Request $request
	 *
	 * @return array|array[]
	 */
	public function addLocationReviews ( \WP_REST_Request $request ): array {
		$params = $request->get_params();
		$post   = get_post( $params['post_id'] );
		
		if ( empty( $params['post_id'] ) || $post->post_type !== JWA_LOCATION_POST_TYPE ) {
			return [ 'code' => 404, 'message' => __( 'Location ID not transferred or not find', 'jwa_locator' ) ];
		}
		
		if ( ! self::tokenChecks( $request ) ) {
			return [ 'code' => 403, 'message' => __( 'Access Denied check the validity of the the token', 'jwa_locator' ) ];
		}
		$commentIDs = [];
		foreach ( $params['reviews'] as $param ) {
			$reviewArray  = [
				'post_id'      => $params['post_id'],
				'user_name'    => $param['reviewer'],
				'user_email'   => ! empty( $param['email'] ) ? $param['email'] : '',
				'content'      => htmlspecialchars( $param['text'] ),
				'comment_type' => 'review_location',
				'user_id'      => 0,
				'status'       => ( $param['status'] == 'publish' ? 'publish' : 'draft' ),
				'rete'         => $param['rete'],
				'date'         => $param['date'],
				'avatar'       => $param['avatar'],
			];
			$newComment   = new jwaLocationReview( $reviewArray );
			$commentIDs[] = $newComment->insertReview( true );
		}
		
		return [ 'comment_id' => $commentIDs ];
	}
	
	/**
	 * Token access
	 *
	 * @param \WP_REST_Request $request
	 *
	 * @return bool
	 */
	public function tokenChecks ( \WP_REST_Request $request ): bool {
		$token = $request['token'];
		
		return $token == JWA_REST_TOKEN;
	}
}