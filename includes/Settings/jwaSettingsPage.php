<?php
/**
 * Created 17.03.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */

namespace JWA_Locator\Settings;

if ( ! defined( 'ABSPATH' ) ) {
	die ( 'Silly human what are you doing here' );
}

class jwaSettingsPage {
	public function __construct () {
		self::addAdminMenu();
	}
	
	/**
	 * Add menu page Theme Settings
	 */
	public function addAdminMenu () {
		
		add_submenu_page( 'edit.php?post_type=' . JWA_LOCATION_POST_TYPE, 'Settings', 'Settings', 'manage_options',
			'location-settings', [
				$this,
				'templateAdminPage',
			] );
	}
	
	/**
	 * Output template page
	 */
	public function templateAdminPage () {
		include_once JWA_LOCATION_PLUGIN_DIR . '/includes/template/admin/settingsPage/template.php';
	}
}