<?php
/**
 * Created 18.03.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */

namespace JWA_Locator\Settings;
if ( ! defined( 'ABSPATH' ) ) {
	die ( 'Silly human what are you doing here' );
}

class jwaSettingsField {
	public function __construct () {
		self::registerSettings();
		self::displaySettingSection();
	}
	
	/**
	 * Register a setting
	 */
	public static function registerSettings () {
		register_setting( 'jwa_location_group', 'jwa_location' );
	}
	
	public function displaySettingSection () {
		add_settings_section( 'jwa_location_g_api', __( 'Google API', 'jwa_locator' ), null, 'location-settings' );
		add_settings_section( 'jwa_location_rest', __( 'REST API', 'jwa_locator' ), null, 'location-settings' );
		add_settings_section( 'jwa_location_post_type', __( 'Post Type Settings', 'jwa_locator' ), [
			$this,
			'postTypeDanger',
		],
			'location-settings' );
		
		
		$fields = [
			[
				'type_field'  => 'text',
				'section'     => 'jwa_location_g_api',
				'id'          => 'g_map_key',
				'page'        => 'location-settings',
				'name'        => __( 'Google Map Key', 'jwa_locator' ),
				'description' => __( 'Enter a Google Map Key', 'jwa_locator' ),
			],
			[
				'type_field'  => 'text',
				'section'     => 'jwa_location_g_api',
				'id'          => 'default_by_canada',
				'page'        => 'location-settings',
				'name'        => __( 'Default City By Canada', 'jwa_locator' ),
				'description' => __( 'Enter a Default City By Canada', 'jwa_locator' ),
			],
			[
				'type_field'  => 'text',
				'section'     => 'jwa_location_g_api',
				'id'          => 'default_by_usa',
				'page'        => 'location-settings',
				'name'        => __( 'Default City By USA', 'jwa_locator' ),
				'description' => __( 'Enter a Default City By USA', 'jwa_locator' ),
			],
			[
				'type_field'  => 'text',
				'section'     => 'jwa_location_rest',
				'id'          => 'token',
				'page'        => 'location-settings',
				'name'        => __( 'Token by REST API', 'jwa_car' ),
				'description' => __( 'Enter a Token', 'jwa_car' ),
			],
			[
				'type_field' => 'button',
				'section'    => 'jwa_location_rest',
				'id'         => 'token_generate',
				'page'       => 'location-settings',
				'name'       => __( 'Generate Token', 'jwa_car' ),
			],
			[
				'type_field'  => 'text',
				'section'     => 'jwa_location_post_type',
				'id'          => 'post_type_name',
				'page'        => 'location-settings',
				'name'        => __( 'Post Type Name', 'jwa_car' ),
				'description' => __( 'Enter a Post Type Name', 'jwa_car' ),
			],
			[
				'type_field'  => 'text',
				'section'     => 'jwa_location_post_type',
				'id'          => 'post_menu_name',
				'page'        => 'location-settings',
				'name'        => __( 'Menu Name', 'jwa_car' ),
				'description' => __( 'Enter a Menu Name', 'jwa_car' ),
			],
			[
				'type_field'  => 'text',
				'section'     => 'jwa_location_post_type',
				'id'          => 'post_archive_slug',
				'page'        => 'location-settings',
				'name'        => __( 'Archive Slug', 'jwa_car' ),
				'description' => __( 'Enter a Archive Slug', 'jwa_car' ),
			],
		];
		self::addSettingField( $fields );
	}
	
	
	/**
	 * Returns all theme options
	 */
	private static function getOptionsSettings () {
		return get_option( 'jwa_location', false );
	}
	
	/**
	 * Add field function
	 *
	 * @param array $fields
	 */
	public function addSettingField ( array $fields ) {
		foreach ( $fields as $field ) {
			add_settings_field( $field['id'], $field['name'], [
				$this,
				'fieldTemplate',
			], $field['page'], $field['section'],
				$field );
		}
	}
	
	/**
	 * Output field
	 *
	 * @param array $args
	 */
	public function fieldTemplate ( array $args ) {
		$option = self::getOptionsSettings();
		switch ( $args['type_field'] ) {
			case 'text':
				echo ' <input style="min-width: 300px;" type="text" name="jwa_location[' . $args['id'] . ']" id="' . $args['id'] . '" value="' . (
					$option ? $option[ $args['id'] ] : '' ) . '" />';
				echo '<p class="description" > ' . $args['description'] . ' </p > ';
				break;
			case 'select-page':
				$html = ' <select name="jwa_location[' . $args['id'] . ']" id ="' . $args['id'] . '">';
				$html .= '<option value=""> ' . __( 'Select Form page Apply', 'jwa_car' ) . ' </option > ';
				
				$pages = self::getPagesName();
				
				if ( $pages ) {
					foreach ( $pages as $key => $page ) {
						$html .= '<option value="' . $key . '" ' . ( $option['page_form'] == $key ? 'selected' : '' ) . '> ' . $page . ' </option > ';
					}
				}
				$html .= '</select > ';
				$html .= '<p class="description" > ' . $args['description'] . ' </p> ';
				echo $html;
				break;
			case 'hidden':
				echo ' <input type="hidden" name="jwa_location[' . $args['id'] . ']" id="' . $args['id'] . '" value="' . (
					$option ? $option[ $args['id'] ] : '' ) . '" />';
				break;
			case 'button':
				echo '<button  id="' . $args['id'] . '" class="button button-secondary">' . $args['name'] . '</button>';
				break;
			case 'select':
				$html = ' <select name="jwa_location[' . $args['id'] . ']" id ="' . $args['id'] . '">';
				foreach ( $args['options'] as $key => $style ) {
					$html .= '<option value="' . $key . '" ' . ( $option['preset'] == $key ? 'selected' : '' ) . '> ' . $style . ' </option > ';
				}
				$html .= '</select > ';
				$html .= '<p class="description" > ' . $args['description'] . ' </p> ';
				echo $html;
				break;
			case 'radio':
				$html = '<div>';
				foreach ( $args['options'] as $key => $item ) {
					$html .= '<label><input name="jwa_location[' . $args['id'] . ']" type="radio" value="' . $key . '" ' . (
						$option['container'] == $key ? 'checked' : '' ) . '>' .
					         $item
					         . '</label><br>';
				}
				$html .= '</div>';
				echo $html;
				break;
		}
	}
	
	/**
	 * Danger Message
	 */
	public function postTypeDanger () {
		echo "<div id='message' class='notice notice-warning'><p><strong>Attention!!! if you write down this field all past data will be lost <br>Before applying the settings, make a full backup of the database.<br>This setting will only affect the recording type location</strong></p></div>";
	}
}