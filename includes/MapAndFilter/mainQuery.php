<?php
/**
 * Created 24.02.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */

namespace JWA_Locator\MapAndFilter;

if ( ! defined( 'ABSPATH' ) ) {
	die ( 'Silly human what are you doing here' );
}

class mainQuery {
	private $db;
	private $code_match = [
		'"',
		'!',
		'@',
		'#',
		'$',
		'%',
		'^',
		'&',
		'*',
		'(',
		')',
		'_',
		'+',
		'{',
		'}',
		'|',
		':',
		'"',
		'<',
		'>',
		'?',
		'[',
		']',
		';',
		"'",
		',',
		'.',
		'/',
		'',
		'~',
		'`',
		'=',
	];
	
	public function __construct () {
		global $wpdb;
		
		$this->db = $wpdb;
		
		/**
		 * Listing search ajax
		 */
		add_action( 'wp_ajax_jwa_get_location_by_coordinate', [ $this, 'getLocationByAjax' ] );
		add_action( 'wp_ajax_nopriv_jwa_get_location_by_coordinate', [ $this, 'getLocationByAjax' ] );
		
		/**
		 * ajax info location
		 */
		add_action( 'wp_ajax_get_location_info', [ $this, 'getInfoByAjax' ] );
		add_action( 'wp_ajax_nopriv_get_location_info', [ $this, 'getInfoByAjax' ] );
		
		add_action( 'admin_post_nopriv_jwa_location_filter', [ $this, 'locationFilterHandler' ] );
		add_action( 'admin_post_jwa_location_filter', [ $this, 'locationFilterHandler' ] );
		
		/**
		 * Set new query var
		 */
		add_filter( 'query_vars', [ $this, 'addFilterVars' ], 10 );
		self::urlRewriteRules();
	}
	
	/**
	 * Listing search ajax
	 *
	 * @return JSON
	 *             content HTML
	 *             count_post int
	 */
	public function getLocationByAjax () {
		$lat      = $_POST['lat'];
		$lng      = $_POST['lng'];
		$address  = $_POST['address'];
		$distance = $_POST['distance'];
		$limit    = 10;
		$location = self::getLocationByCoordinate( (float) $lat, (float) $lng, (int) $distance, (int) $limit );
		$arg      = [
			'post_type'      => JWA_LOCATION_POST_TYPE,
			'posts_per_page' => $limit,
			'post__in'       => $location['IDs'],
			'order'          => 'DESC',
			'orderby'        => 'comment_count',
			'post_status'    => 'publish',
		];
		
		if ( empty( $location['IDs'][0] ) ) {
			wp_send_json_success( [ 'content' => 'Store in this location no found', 'code' => 404 ] );
		} else {
			$item  = [];
			$query = new \WP_Query( $arg );
			while ( $query->have_posts() ) {
				$query->the_post();
				ob_start();
				include JWA_LOCATION_PLUGIN_DIR . '/template/archive/location-item.php';
				$item[] = ob_get_clean();
			}
			wp_reset_postdata();
			wp_send_json_success( [ 'content' => $item, "count_post" => $query->found_posts, 'code' => 200 ] );
		}
	}
	
	public function getInfoByAjax () {
		$postID = $_POST['id'];
		$arg    = [
			'post_type' => JWA_LOCATION_POST_TYPE,
			'p'         => $postID,
		];
		$query  = new \WP_Query( $arg );
		$item   = '';
		while ( $query->have_posts() ) {
			$query->the_post();
			ob_start();
			include JWA_LOCATION_PLUGIN_DIR . '/template/archive/location-info.php';
			$item = ob_get_clean();
		}
		wp_reset_postdata();
		
		wp_send_json_success( [ 'content' => $item ] );
	}
	
	/**
	 * Searches the database for locations within a certain radius by coordinates
	 *
	 * @param float $lat
	 * @param float $lng
	 * @param int   $distance
	 * @param int   $limit
	 *
	 * @return array
	 *              Distance -> Distance from the center of the radius
	 *              IDs -> Post ID from query
	 *
	 */
	public function getLocationByCoordinate ( float $lat, float $lng, int $distance, int $limit = null ): array {
		$distance --;
		
		
		$prefix    = $this->db->prefix;
		$sql       = "SELECT *,
								p.distance_unit
								* DEGREES(ACOS(LEAST(1.0, COS(RADIANS(p.latpoint))
								* COS(RADIANS(z.lat))
								* COS(RADIANS(p.longpoint) - RADIANS(z.lng))
								+ SIN(RADIANS(p.latpoint))
								* SIN(RADIANS(z.lat))))) AS distance_in_km
								FROM {$prefix}jwa_location AS z
								JOIN (   /* these are the query parameters */
								SELECT  {$lat}  AS latpoint,  {$lng} AS longpoint,
								{$distance} AS radius,      111.045 AS distance_unit
								) AS p ON 1=1
								WHERE z.lat
								BETWEEN p.latpoint  - (p.radius / p.distance_unit)
								AND p.latpoint  + (p.radius / p.distance_unit)
								AND z.lng
								BETWEEN p.longpoint - (p.radius / (p.distance_unit * COS(RADIANS(p.latpoint))))
								AND p.longpoint + (p.radius / (p.distance_unit * COS(RADIANS(p.latpoint))))
								ORDER BY distance_in_km";
		$response  = $this->db->get_results( $sql, OBJECT );
		$locations = [];
		
		foreach ( $response as $key => $location ) {
			$locations["distance_location"][ $location->post_id ]['distance'] = $location->distance_in_km;
			$locations['IDs'][]                                               = $location->post_id;
		}
		
		return $locations;
	}
	
	/**
	 * Get Coordinate by local address
	 *
	 * @param $address
	 *
	 * @return array
	 */
	public function getCoordinateByAddress ( $address ): array {
		
		
		$url = "https://maps.google.com/maps/api/geocode/json?address=" . $address . "&sensor=false&language=en&key=" .
		       JWA_LOCATION_GOOGLE_KEY;
		
		$responce = wp_remote_get( $url, [
			'sslverify' => false,
			'timeout'   => 20,
			'method'    => 'GET',
		] );
		$data     = json_decode( $responce['body'] );
		
		
		return [ 'lat' => $data->results[0]->geometry->location->lat, 'lng' => $data->results[0]->geometry->location->lng ];
	}
	
	/**
	 * Create Url redirect
	 */
	public function locationFilterHandler () {
		$request = $_REQUEST['jwa_location'];
		
		$cityTerm = get_term_by( 'name', $request['city'], 'area' );
		
		$redirect = get_bloginfo( 'url' ) . '/area/' . $request['province'] . '/' . ( ! empty( $cityTerm ) ?
				$cityTerm->slug . '/filter' : 'filter' ) . '/';
		$params   = [];
		foreach ( $request['filter'] as $key => $item ) {
			if ( ! empty( $item ) ) {
				if ( is_string( $item ) ) {
					$params[ $key ] = urlencode( $item );
				} else {
					$params[ $key ] = str_replace( $this->code_match, '-', $item );
				}
			}
			
			if ( $key !== 'address' ) {
				$params[ $key ] = array_unique( $params[ $key ] );
			}
		}
		
		// if no param back to city or province page
		if ( empty( $params ) ) {
			$redirect = get_bloginfo( 'url' ) . '/area/' . $request['province'] . '/' . ( ! empty( $request['city'] ) ?
					$cityTerm->slug : '' ) . '/';
			wp_redirect( $redirect, 302 );
			die();
		}
		
		$i = 0;
		foreach ( $params as $key => $param ) {
			if ( $i == 0 ) {
				if ( is_array( $param ) ) {
					foreach ( $param as $k => $value ) {
						if ( $k == 0 ) {
							$redirect .= $key . '-in-' . $value;
						} else {
							$redirect .= '-or-' . $value;
						}
					}
				} else {
					$redirect .= $key . '-in-' . $param;
				}
			} else {
				if ( ! is_array( $param ) ) {
					$redirect .= '-and-' . $key . '-in-' . $param;
				} else {
					foreach ( $param as $i => $item ) {
						if ( $i == 0 ) {
							$redirect .= '-and-' . $key . '-in-' . $item;
						} else {
							$redirect .= '-or-' . $item;
						}
					}
				}
			}
			$i ++;
		}
		wp_redirect( $redirect, 302 );
		die();
	}
	
	/**
	 * Add Query vars filter
	 *
	 * @param $vars
	 *
	 * @return mixed
	 */
	public function addFilterVars ( $vars ) {
		$vars[] = 'filter';
		
		return $vars;
	}
	
	/**
	 * Add Rewrite rules url
	 */
	public function urlRewriteRules () {
		
		add_rewrite_rule(
			'area/(.+?)/filter/([-_a-zA-Z0-9+%.:]+)/page/(\d+)/?$',
			'index.php?post_type=' . JWA_LOCATION_POST_TYPE . '&area=$matches[1]&filter=$matches[2]&paged=$matches[3]',
			'top'
		);
		
		add_rewrite_rule(
			"area/(.+?)/filter/([-_a-zA-Z0-9+%.:]+)/?$",
			'index.php?post_type=' . JWA_LOCATION_POST_TYPE . '&area=$matches[1]&filter=$matches[2]',
			'top'
		);
		
		add_rewrite_rule(
			"area/filter/([-_a-zA-Z0-9+%.:]+)/?$",
			'index.php?post_type=' . JWA_LOCATION_POST_TYPE . '&taxonomy=area&filter=$matches[1]',
			'top'
		);
	}
	
	/**
	 * Parse string URL
	 *
	 * @param string $urlQuery
	 *
	 * @return array|false
	 */
	public function parseURLQuery ( string $urlQuery ) {
		$paramString = $urlQuery;
		$queryArg    = [];
		if ( ! empty( $paramString ) ) {
			$params = explode( '-and-', $paramString );
			
			foreach ( $params as $param ) {
				
				$item = explode( '-in-', $param );
				
				if ( preg_match( '-or-', $item[1] ) ) {
					$items                = explode( '-or-', $item[1] );
					$queryArg[ $item[0] ] = $items;
				} else {
					$queryArg[ $item[0] ][] = $item[1];
				}
				
			}
			
			return $queryArg;
		} else {
			return false;
		}
	}
	
	/**
	 * Get Query Arguments
	 *
	 * @param array $params
	 * @param array $locationIDs
	 * @param null  $locationAreasID
	 *
	 * @return array
	 */
	public function getQueryArg ( array $params, array $locationIDs, $locationAreasID = null ): array {
		if ( isset( $params['category'] ) && ! is_array( $params['category'] ) ) {
			$params['category'] = [ $params['category'] ];
		}
		
		$arg = [
			'post_type'      => JWA_LOCATION_POST_TYPE,
			'posts_per_page' => 10,
			'paged'          => get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1,
			'order'          => 'DESC',
			'post__in'       => $locationIDs,
			'orderby'        => 'post__in',
			'post_status'    => 'publish',
		];
		
		if ( isset( $locationAreasID ) && ! empty( $locationAreasID ) ) {
			$arg['tax_query'] = [
				'relation' => "AND",
				[
					'taxonomy'         => 'area',
					'field'            => 'id',
					'terms'            => [ $locationAreasID ],
					'include_children' => false,
				],
			];
		}
		
		
		if ( isset( $params['category'] ) && ! empty( $params['category'][0] ) ) {
			$arg['tax_query'][] = [
				[
					'taxonomy' => 'location-category',
					'field'    => 'slug',
					'terms'    => $params['category'][0],
					'operator' => ( count( $params['category'] ) > 1 ? 'OR' : 'AND' ),
				],
			];
		}
		
		if ( isset( $params['tags'] ) && ! empty( $params['tags'] ) ) {
			$arg['tax_query'][] = [
				[
					'taxonomy' => 'tag-locations',
					'field'    => 'slug',
					'terms'    => $params['tags'],
					'operator' => 'AND',
				],
			];
		}
		
		return $arg;
	}
}