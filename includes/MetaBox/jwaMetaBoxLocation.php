<?php
/**
 * Created 16.01.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */

namespace JWA_Locator\MetaBox;

use JWA\Car\Helpers\biWeekly\jwaBiWeekly;
use JWA_Locator\Helpers\jwaPostData;

if ( ! defined( 'ABSPATH' ) ) {
	die ( 'Silly human what are you doing here' );
}

class jwaMetaBoxLocation {
	private $postTypeName;
	private $helpers;
	
	public function __construct ( string $postTypeName ) {
		$this->postTypeName = $postTypeName;
		$this->helpers      = new jwaPostData();
		
		add_action( 'add_meta_boxes', [ $this, 'addMetaBox' ] );
		add_action( 'admin_enqueue_scripts', [ $this, 'addScriptsAdmin' ] );
		
		add_action( 'save_post', [ $this, 'savePostMeta' ], 10, 3 );
		
		//ajax
		add_action( 'wp_ajax_set_review_status', [ $this, 'setAjaxReviewStatus' ] );
		add_action( 'wp_ajax_nopriv_set_review_status', [ $this, 'setAjaxReviewStatus' ] );
		
		add_action( 'wp_ajax_delete_review', [ $this, 'deleteAjaxReview' ] );
		add_action( 'wp_ajax_nopriv_delete_review', [ $this, 'deleteAjaxReview' ] );
	}
	
	/**
	 * Add meta box
	 */
	public function addMetaBox () {
		add_meta_box( 'jwa_locator_metabox', __( 'Location', 'jwa_locator' ), [
			$this,
			'outputMetaBox',
		], [ $this->postTypeName ], 'advanced', 'high' );
	}
	
	/**
	 * Output meta box template
	 */
	public function outputMetaBox () {
		include_once JWA_LOCATION_PLUGIN_DIR . '/includes/template/admin/metabox/post_type/template.php';
	}
	
	/**
	 * Add Style & Scripts in admin panel
	 *
	 * @param $hook_suffix
	 */
	public function addScriptsAdmin ( $hook_suffix ) {
		
		if ( $hook_suffix == 'post-new.php' && $_GET['post_type'] == $this->postTypeName || $hook_suffix == 'post.php' ) {
			
			if ( ! did_action( 'wp_enqueue_media' ) ) {
				wp_enqueue_media();
			}
			
			wp_enqueue_script( 'bootstrap', JWA_LOCATION_PLUGIN_URL . '/assets/js/admin/metabox/bootstrap.min.js', [ 'jquery' ],
				$this->helpers->versionFile( '/assets/js/admin/metabox/bootstrap.min.js' ),
				true );
			wp_enqueue_style( 'metabox_style', JWA_LOCATION_PLUGIN_URL . '/assets/css/admin/main.css', '',
				$this->helpers->versionFile( '/assets/css/main.css' ) );
			
			wp_enqueue_script( 'g-map', "//maps.googleapis.com/maps/api/js?key=" . JWA_LOCATION_GOOGLE_KEY . '&libraries=places&language=en', [ 'jquery' ],
				'1.0.0',
				true );
			wp_enqueue_script( 'metabox-main', JWA_LOCATION_PLUGIN_URL . '/assets/js/admin/metabox/main.js', [ 'jquery' ],
				$this->helpers->versionFile( '/assets/js/admin/metabox/main.js' ),
				true );
			
			wp_localize_script( 'metabox-main', 'jwa_locator', [ 'url_ajax' => admin_url( 'admin-ajax.php' ) ] );
			
			wp_enqueue_style( 'bootstrap', '//cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css', '', JWA_LOCATION_VERSION );
		}
	}
	
	/**
	 * Save Meta Box Settings
	 *
	 * @param $post_ID
	 * @param $post
	 * @param $update
	 */
	public function savePostMeta ( $post_ID, $post, $update ) {
		$slug = $this->postTypeName;
		
		if ( $slug != $_POST['post_type'] ) {
			return;
		}
		
		if ( ! isset( $_POST['jwa_location_lat'] ) ) {
			return;
		}
		
		$dataArray    = self::gatMetaBoxDataArray( $_POST );
		$locationData = [];
		foreach ( $dataArray as $key => $value ) {
			switch ( $key ) {
				case 'jwa_location_address':
					$locationData['address'] = $value;
					break;
				case 'jwa_location_postal_code':
					$locationData['postal_code'] = $value;
					break;
				case 'jwa_location_country':
					$locationData['country'] = $value;
					break;
				case 'jwa_location_province':
					$locationData['province'] = $value;
					$province                 = $value;
					break;
				case 'jwa_location_lat':
					$locationData['lat'] = $value;
					break;
				case 'jwa_location_lng':
					$locationData['lng'] = $value;
					break;
				case 'jwa_location_city':
					$city = $value;
					break;
				default:
					update_post_meta( $post_ID, $key, $value );
					break;
			}
		}
		
		$this->helpers->insertToLocationTable( $post_ID, $locationData );
		if ( isset( $province ) && ! empty( $province ) && ! empty( $city ) ) {
			$provinceID = $this->helpers->setTerm( $post_ID, $province, 'area' );
			$this->helpers->setTerm( $post_ID, $city, 'area', $provinceID );
		}
	}
	
	/**
	 * Create Array Meta Box Field Value
	 *
	 * @param array $data
	 *
	 * @return array
	 */
	private function gatMetaBoxDataArray ( array $data ) {
		$metaBoxData = [];
		foreach ( $data as $key => $value ) {
			if ( preg_match( '/jwa_location_*/', $key ) && ! empty( $value ) ) {
				$metaBoxData[ $key ] = $value;
			} else if ( preg_match( '/jwa_location_*/', $key ) && empty( $value ) ) {
				$metaBoxData[ $key ] = '';
			}
		}
		
		return $metaBoxData;
	}
	
	/**
	 * Ajax Change Status
	 */
	public function setAjaxReviewStatus () {
		$idReview = $_POST['id'];
		$postID   = $_POST['postID'];
		$status   = $_POST['status'];
		
		$review = $this->helpers->setReviewStatus( (int) $idReview, (int) $postID, $status );
		if ( $review ) {
			wp_send_json_success( [ 'status' => $review ] );
		} else {
			wp_send_json_error( [ 'status' => 'error' ] );
		}
	}
	
	
	/**
	 * Ajax Delete Review
	 */
	public function deleteAjaxReview () {
		$idReview = $_POST['id'];
		$postID   = $_POST['postID'];
		
		$review = $this->helpers->deleteReview( (int) $idReview, (int) $postID );
		
		if ( $review ) {
			wp_send_json_success( [ 'status' => $review ] );
		} else {
			wp_send_json_error( [ 'status' => 'error' ] );
		}
	}
	
}