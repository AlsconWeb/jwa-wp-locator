<?php
/**
 * Created 11.02.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */

namespace JWA_Locator\CommentForm;

use JWA_Locator\Helpers\jwaLocationReview;

if ( ! defined( 'ABSPATH' ) ) {
	die ( 'Silly human what are you doing here' );
}

class jwaCommentForm {
	
	/**
	 * jwaCommentForm constructor.
	 *
	 * set action:
	 * admin_post_nopriv
	 * admin_post
	 *
	 * set filter:
	 * preprocess_comment
	 */
	
	
	public function __construct () {
		add_action( 'wp_ajax_nopriv_jwa_location_reviews', [ $this, 'saveComment' ] );
		add_action( 'wp_ajax_jwa_location_reviews', [ $this, 'saveComment' ] );
		add_action( 'transition_comment_status', [ $this, 'updateReviewStatus' ], 10, 3 );
		add_filter( 'comments_template', [ $this, 'commentFormTemplate' ] );
		
	}
	
	/**
	 * Add template comment from
	 *
	 * @return false|string
	 */
	public function getCommentForm () {
		ob_start();
		include JWA_LOCATION_PLUGIN_DIR . '/template/commentForm/template.php';
		
		return ob_get_clean();
	}
	
	/**
	 * Ajax save review
	 */
	public function saveComment () {
		
		$dataArr = [
			'content'    => $_POST['content'],
			'post_id'    => $_POST['post_id'],
			'user_id'    => $_POST['user_id'],
			'rete'       => $_POST['rete'],
			'user_name'  => $_POST['user_name'],
			'user_email' => $_POST['user_email'],
		];
		
		if ( empty( $_POST['rete'] ) ) {
			wp_send_json_error( [ 'message' => __( 'You have not completed the rating', 'jwa_location' ) ] );
		}
		
		if ( $_POST['user_id'] == 0 && empty( $_POST['user_name'] ) ) {
			wp_send_json_error( [ 'message' => __( 'Fill in the Name field', 'jwa_location' ) ] );
		}
		
		$insertComment = new jwaLocationReview( $dataArr );
		$id            = $insertComment->insertReview();
		
		if ( $id ) {
			wp_send_json_success( [
				'comment_id' => $id,
				'message'    => 'Your review has been sent for moderation',
			] );
		} else {
			wp_send_json_error( [ 'message' => __( 'Something went wrong, please try again later', 'jwa_location' ) ] );
		}
		
	}
	
	/**
	 * Update Review Status
	 *
	 * @param $newStatus
	 * @param $oldStatus
	 * @param $comment
	 */
	public function updateReviewStatus ( $newStatus, $oldStatus, $comment ) {
		if ( $oldStatus != $newStatus ) {
			if ( $newStatus == 'approved' ) {
				$updateStatus = new jwaLocationReview();
				
				$status = $updateStatus->changeStatus( 'publish', $comment->comment_ID );
				
				if ( ! $status ) {
					add_action( 'admin_notices', function () {
						echo '<div class="notice notice-error is-dismissible"><p>Status has not been updated</p></div>';
					} );
				}
			} else {
				$updateStatus = new jwaLocationReview();
				$status       = $updateStatus->changeStatus( 'draft', $comment->comment_ID );
				
				if ( ! $status ) {
					
					add_action( 'admin_notices', function () {
						echo '<div class="notice notice-error is-dismissible"><p>Status has not been updated</p></div>';
					} );
				}
			}
		}
	}
	
	/**
	 * Change Template file
	 *
	 * @param $theme_template
	 *
	 * @return string
	 */
	public function commentFormTemplate (
		$theme_template
	): string {
		global $post;
		if ( $post->post_type == JWA_LOCATION_POST_TYPE ) {
			return JWA_LOCATION_PLUGIN_DIR . '/template/comment/template.php';
		}
		
		return $theme_template;
	}
	
	/**
	 * Comment Walker
	 *
	 * @param $comment
	 * @param $args
	 * @param $depth
	 */
	static public function commentWalker ( $comment, $args, $depth ) {
		$countTop    = get_comment_meta( $comment->comment_ID, 'jwa_top_comment', true );
		$countBottom = get_comment_meta( $comment->comment_ID, 'jwa_button_comment', true );
		
		$countTop    = $countTop == false ? '0' : $countTop;
		$countBottom = $countBottom == false ? '0' : $countBottom;
		$helpers     = new jwaLocationReview();
		?>
		
		<?php if ( $comment->comment_approved == '1' ): ?>
			<li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
				<div id="comment-<?php comment_ID() ?>">
					<div class="comment_block">
						<div class="comment-author vcard">
							<img class="avatar" src="<?php echo $helpers->getAvatarUser( get_comment_ID() ) ?>" alt="#">
							<cite class="fn"><?php echo $comment->comment_author ?></cite>
							<span class="says"></span>
							<span class="time icon-clock"><?php echo $helpers->getCommentData( get_comment_ID() )
								?></span>
						</div>
						<div class="comment_text">
							<?php comment_text() ?>
						</div>
						<ul class="sympathy" data-comment_id="<?php echo $comment->comment_ID ?>">
							<li><a class="icon-top" href="#"><?php echo $countTop; ?></a></li>
							<li><a class="icon-bottom" href="#"><?php echo $countBottom; ?></a></li>
						</ul>
						<div class="reply">
							<!--								--><?php //comment_reply_link( array_merge( $args, [
							//									'depth'     => $depth,
							//									'max_depth' => $args['max_depth'],
							//								] ) ) ?>
						</div>
						<div class="clear"></div>
					</div>
				</div>
			</li>
		<?php endif; ?>
		<?php
	}
	
}