<?php
/**
 * Created 12.02.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */

namespace JWA_Locator\Helpers;
if ( ! defined( 'ABSPATH' ) ) {
	die ( 'Silly human what are you doing here' );
}

class jwaLocationReview {
	private $commentData;
	private $db;
	
	/**
	 * jwaLocationReview constructor.
	 *
	 * @param null $commentData
	 */
	public function __construct ( $commentData = null ) {
		global $wpdb;
		$this->commentData = $commentData;
		$this->db          = $wpdb;
	}
	
	/**
	 * insert comment
	 *
	 * @return false|int|string|\WP_Error
	 */
	public function insertReview ( bool $rest = null ) {
		if ( ! $rest ) {
			if ( $this->commentData['user_id'] == 0 ) {
				$data = [
					'comment_post_ID'      => $this->commentData['post_id'],
					'comment_author'       => $this->commentData['user_name'],
					'comment_author_email' => $this->commentData['user_email'],
					'comment_author_url'   => '',
					'comment_content'      => $this->commentData['content'],
					'comment_type'         => 'review_location',
					'user_id'              => 0,
					'comment_approved'     => ( isset( $this->commentData['status'] ) && $this->commentData['status'] == 'publish' ? 1 : 0 ),
				];
			} else {
				$userData = get_user_meta( $this->commentData['user_id'], 'nickname', true );
				
				$data = [
					'comment_post_ID'    => $this->commentData['post_id'],
					'comment_author'     => $userData,
					'comment_author_url' => '',
					'comment_content'    => $this->commentData['content'],
					'comment_type'       => 'review_location',
					'user_id'            => $this->commentData['user_id'],
					'comment_approved'   => ( isset( $this->commentData['status'] ) && $this->commentData['status'] == 'publish' ? 1 : 0 ),
				];
			}
			$commentID = wp_new_comment( $data, true );
		}
		
		if ( $rest ) {
			$data      = [
				'comment_post_ID'      => $this->commentData['post_id'],
				'comment_author'       => $this->commentData['user_name'],
				'comment_author_email' => $this->commentData['user_email'],
				'comment_author_url'   => '',
				'comment_content'      => $this->commentData['content'],
				'comment_type'         => 'review_location',
				'user_id'              => 0,
				'comment_approved'     => ( isset( $this->commentData['status'] ) && $this->commentData['status'] == 'publish' ? 1 : 0 ),
			];
			$commentID = wp_insert_comment( $data );
		}
		
		
		if ( $commentID && ! is_wp_error( $commentID ) ) {
			
			$prefix = $this->db->base_prefix;
			$data   = [
				'post_id'    => (int) $this->commentData['post_id'],
				'rating'     => (int) $this->commentData['rete'],
				'comment_id' => (int) $commentID,
				'status'     => ( isset( $this->commentData['status'] ) && $this->commentData['status'] == 'publish' ? 'publish' : 'draft' ),
				'date_post'  => ( ! empty( $this->commentData['date'] ) ? $this->commentData['date'] : date( "Y-m-d" ) ),
				'avatar'     => ( ! empty( $this->commentData['avatar'] ) ? $this->commentData['avatar'] : 'https://i.pravatar.cc/64' ),
			];
			$format = [
				'%d',
				'%d',
				'%d',
				'%s',
				'%s',
				'%s',
			];
			
			$res = $this->db->insert( "{$prefix}jwa_rating_location", $data, $format );
			
			return $commentID;
		}
		
		return false;
	}
	
	/**
	 * Update status comment in custom table
	 *
	 * @param $newStatus
	 * @param $commentID
	 *
	 * @return bool
	 */
	public function changeStatus ( $newStatus, $commentID ): bool {
		$prefix = $this->db->base_prefix;
		$update = $this->db->update( "{$prefix}jwa_rating_location",
			[ 'status' => $newStatus ],
			[ 'comment_id' => $commentID ],
			[ '%s' ], [ '%d' ] );
		
		if ( $update && $update !== 0 ) {
			return $update;
		} else {
			return false;
		}
	}
	
	/**
	 * Count Comment Approved
	 *
	 * @param $postID
	 *
	 * @return mixed
	 */
	public function getCountComment ( $postID ) {
		return wp_count_comments( $postID )->approved;
	}
	
	/**
	 * Get url avatar user
	 *
	 * @param $commentID
	 *
	 * @return mixed
	 */
	public function getAvatarUser ( $commentID ) {
		$prefix   = $this->db->prefix;
		$sql      = "SELECT * FROM `{$prefix}jwa_rating_location` WHERE `comment_id` = {$commentID}";
		$response = $this->db->get_results( $sql, OBJECT );
		
		return $response[0]->avatar;
	}
	
	/**
	 * Return in date post comment
	 *
	 * @param $commentID
	 *
	 * @return false|int|string
	 */
	public function getCommentData ( $commentID ) {
		$prefix   = $this->db->prefix;
		$sql      = "SELECT * FROM `{$prefix}jwa_rating_location` WHERE `comment_id` = {$commentID}";
		$response = $this->db->get_results( $sql, OBJECT );
		
		return mysql2date( 'F j, Y', $response[0]->date_post );
	}
	
	/**
	 * Get star rating location
	 *
	 * @param $postID
	 *
	 * @return float
	 */
	public function getStarRating ( $postID ): float {
		$prefix   = $this->db->prefix;
		$sql      = "SELECT AVG(rating) AS rating FROM {$prefix}jwa_rating_location WHERE post_id IN ({$postID}) AND status = 'publish'";
		$response = $this->db->get_results( $sql, OBJECT );
		
		return round( $response[0]->rating, 1, PHP_ROUND_HALF_DOWN );
	}
	
	
	/**
	 * Get Rating Data from Cart
	 *
	 * @param $postID
	 *
	 * @return array
	 */
	public function getRatingChartData ( $postID ): array {
		$total = self::getCountComment( $postID );
		$five  = self::getRatingStar( 5, $postID );
		$four  = self::getRatingStar( 4, $postID );
		$three = self::getRatingStar( 3, $postID );
		$two   = self::getRatingStar( 2, $postID );
		$one   = self::getRatingStar( 1, $postID );
		
		return [
			'reviews'      => [
				'five'  => [ 'star' => $five, 'percent' => self::getPercentToProgressBar( $total, $five ) ],
				'four'  => [ 'star' => $four, 'percent' => self::getPercentToProgressBar( $total, $four ) ],
				'three' => [ 'star' => $three, 'percent' => self::getPercentToProgressBar( $total, $three ) ],
				'two'   => [ 'star' => $two, 'percent' => self::getPercentToProgressBar( $total, $two ) ],
				'one'   => [ 'star' => $one, 'percent' => self::getPercentToProgressBar( $total, $one ) ],
			],
			'all_comments' => $total,
		];
	}
	
	/**
	 * Get Count star review
	 *
	 * @param int $countStar
	 * @param     $postID
	 *
	 * @return int
	 */
	private function getRatingStar ( int $countStar, $postID ) {
		$prefix   = $this->db->prefix;
		$sql      = "SELECT COUNT(*) FROM {$prefix}jwa_rating_location WHERE `rating` = {$countStar} AND status = 'publish' AND post_id = {$postID}";
		$response = $this->db->get_results( $sql, ARRAY_N );
		
		return (int) $response[0][0];
	}
	
	/**
	 * Get Percent to progress bar
	 *
	 * @param $total
	 * @param $value
	 *
	 * @return float|int
	 */
	public function getPercentToProgressBar ( $total, $value ) {
		if ( empty( $value ) ) {
			return 0;
		}
		
		return ( 100 * $value ) / $total;
	}
	
	/**
	 * Get Rating In Comments
	 *
	 * @param $commentID
	 *
	 * @return int
	 */
	public function getRatingInComments ( $commentID ) {
		$prefix   = $this->db->prefix;
		$sql      = "SELECT * FROM {$prefix}jwa_rating_location WHERE `comment_id` = {$commentID}";
		$response = $this->db->get_results( $sql, OBJECT );
		
		return $response[0]->rating;
	}
}