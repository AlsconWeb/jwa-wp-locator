<?php
/**
 * Created 11.02.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */

namespace JWA_Locator\Helpers;
if ( ! defined( 'ABSPATH' ) ) {
	die ( 'Silly human what are you doing here' );
}

class jwaLocationTime {
	private $helpers;
	private $postID;
	private $dayName = [
		'mon' => 'Monday',
		'tue' => 'Tuesday',
		'wed' => 'Wednesday',
		'thu' => 'Thursday',
		'fri' => 'Friday',
		'sat' => 'Saturday',
		'sun' => 'Sunday',
	];
	private $openHours;
	
	/**
	 * jwaLocationTime constructor.
	 *
	 * @param $postID
	 */
	public function __construct ( $postID ) {
		$this->postID    = $postID;
		$this->helpers   = new jwaPostData();
		$this->openHours = $this->helpers->getMetaByName( 'open_hours', $this->postID );
	}
	
	/**
	 * Get Business Time location
	 *
	 * @return array | false
	 */
	public function getTimeOpen () {
		$businessTime      = $this->openHours;
		$businessTimeArray = [];
		foreach ( $businessTime as $key => $time ) {
			if ( ! isset( $time['open'] ) ) {
				return false;
			}
			$businessTimeArray[] = [
				'day'         => $this->dayName[ $key ],
				'open'        => ( $time['open'] == 'Closed' || empty( $time['open'] ) || empty( $time['close'] ) ? 'Closed' :
					$time['open'] . ' - ' .
					$time['close'] . '' ),
				'current_day' =>
					self::isCurrentDay( $key ),
			];
		}
		if ( ! empty( $businessTimeArray ) ) {
			return $businessTimeArray;
		} else {
			return false;
		}
	}
	
	/**
	 * Current day is day Name
	 *
	 * @param string $dayName
	 *
	 * @return bool
	 */
	private function isCurrentDay ( string $dayName ): bool {
		$dateTime = \DateTime::createFromFormat( 'YmdHi', date( 'YmdHi' ) );
		if ( strtolower( $dateTime->format( 'D' ) ) == $dayName ) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Open Current Day
	 *
	 * @return false|string
	 */
	public function openCurrentDay () {
		if ( ! empty( $this->openHours ) ) {
			foreach ( $this->openHours as $key => $day ) {
				if ( ! isset( $day['close'][0] ) ) {
					return false;
				}
				if ( self::isCurrentDay( $key ) ) {
					return $day['close'];
				}
			}
		} else {
			return false;
		}
	}
	
}