<?php
/**
 * Created 01.03.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */

namespace JWA_Locator\Helpers;

/**
 * Class jwaUserInfo
 *
 * @package JWA_Locator\Helpers
 */
class jwaUserInfo {
	private $lat;
	private $lng;
	protected $ip;
	private $term;

	/**
	 * JwaUserInfo constructor.
	 *
	 * @param string|null $remoteAddr Remote addr.
	 */
	public function __construct( $remoteAddr = null ) {
		$this->ip = $remoteAddr;
	}

	/**
	 * Get location by IP user.
	 *
	 * @param string|null $ip IP.
	 *
	 * @return mixed|false
	 */
	public function getLocationByIP( $ip = null ) {
		if ( empty( $this->ip ) || null === $ip ) {
			return false;
		}

		$this->ip = $ip;

		return unserialize( file_get_contents( 'http://www.geoplugin.net/php.gp?ip=' . $this->ip ) );
	}

	/**
	 * Get default city by user.
	 *
	 * @return array|false
	 */
	public function getDefaultCityByUser() {
		$defaultCities = [
			'CA' => 'toronto',
			'US' => 'new-york',
		];

		$locationData = $this->getLocationByIP( $this->ip );
		$country      = isset( $locationData['geoplugin_countryCode'] ) ? $locationData['geoplugin_countryCode'] : '';
		$city         = isset( $locationData['geoplugin_city'] ) ? $locationData['geoplugin_city'] : '';

		if ( ! array_key_exists( $country, $defaultCities ) ) {
			$country = 'US';
		}

		if ( ! $city ) {
			$city = $defaultCities[ $country ];
		}

		return $this->getCityData( $city );
	}

	/**
	 * Get term by IP.
	 *
	 * @return array|false|\WP_Error|\WP_Term|null
	 */
	public function getTermByIP() {
		$locationData = $this->getLocationByIP( $this->ip );
		if ( ! isset( $locationData['geoplugin_city'] ) ) {
			return false;
		}

		return get_term_by( 'name', $locationData['geoplugin_city'], 'area' );
	}

	/**
	 * Get city data.
	 *
	 * @param string $city City.
	 *
	 * @return array|false
	 */
	protected function getCityData( string $city ) {
		$cityTerm = $this->getCityTerm( $city );

		if ( ! $cityTerm || is_wp_error( $cityTerm ) ) {
			return false;
		}

		$lat = get_term_meta( $cityTerm->term_id, 'jwa_location_lat', true );
		$lng = get_term_meta( $cityTerm->term_id, 'jwa_location_lng', true );

		$cityData            = [];
		$cityData['lat']     = str_replace( ',', '.', $lat );
		$cityData['lng']     = str_replace( ',', '.', $lng );
		$cityData['name']    = $cityTerm->name;
		$cityData['city_id'] = $cityTerm->term_id;

		return $cityData;
	}

	/**
	 * Get city term.
	 *
	 * @param string $city City.
	 *
	 * @return false|\WP_Error|\WP_Term
	 */
	protected function getCityTerm( string $city ) {
		return get_term_by( 'slug', $city, 'area' );
	}
}
