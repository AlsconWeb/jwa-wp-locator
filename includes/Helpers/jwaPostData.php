<?php
/**
 * Created 16.01.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 *
 */

namespace JWA_Locator\Helpers;
if ( ! defined( 'ABSPATH' ) ) {
	die ( 'Silly human what are you doing here' );
}

class jwaPostData {
	private $province = [
		'ON' => 'Ontario',
		'QC' => 'Quebec',
		'NS' => 'Nova Scotia',
		'NB' => 'New Brunswick',
		'MB' => 'Manitoba',
		'BC' => 'British Columbia',
		'PE' => 'Prince Edward Island',
		'SK' => 'Saskatchewan',
		'AB' => 'Alberta',
		'NL' => 'Newfoundland and Labrador',
		// USA
		'AL' => 'Alabama',
		'AK' => 'Alaska',
		'AZ' => 'Arizona',
		'AR' => 'Arkansas',
		'CA' => 'California',
		'CO' => 'Colorado',
		'CT' => 'Connecticut',
		'DE' => 'Delaware',
		'DC' => 'District of Columbia',
		'FL' => 'Florida',
		'GA' => 'Georgia',
		'HI' => 'Hawaii',
		'ID' => 'Idaho',
		'IL' => 'Illinois',
		'IN' => 'Indiana',
		'IA' => 'Iowa',
		'KS' => 'Kansas',
		'KY' => 'Kentucky',
		'LA' => 'Louisiana',
		'ME' => 'Maine',
		'MD' => 'Maryland',
		'MA' => 'Massachusetts',
		'MI' => 'Michigan',
		'MN' => 'Minnesota',
		'MS' => 'Mississippi',
		'MO' => 'Missouri',
		'MT' => 'Montana',
		'NE' => 'Nebraska',
		'NV' => 'Nevada',
		'NH' => 'New Hampshire',
		'NJ' => 'New Jersey',
		'NM' => 'New Mexico',
		'NY' => 'New York',
		'NC' => 'North Carolina',
		'ND' => 'North Dakota',
		'OH' => 'Ohio',
		'OK' => 'Oklahoma',
		'OR' => 'Oregon',
		'PA' => 'Pennsylvania',
		'RI' => 'Rhode Island',
		'SC' => 'South Carolina',
		'SD' => 'South Dakota',
		'TN' => 'Tennessee',
		'TX' => 'Texas',
		'UT' => 'Utah',
		'VT' => 'Vermont',
		'VA' => 'Virginia',
		'WA' => 'Washington',
		'WV' => 'West Virginia',
		'WI' => 'Wisconsin',
		'WY' => 'Wyoming',
	];
	private $db;
	
	
	public function __construct () {
		global $wpdb;
		$this->db = $wpdb;
	}
	
	/**
	 * Add version file
	 *
	 * @param string $filePath
	 *
	 * @return false|int
	 */
	public function versionFile ( string $filePath ) {
		return filemtime( JWA_LOCATION_PLUGIN_DIR . $filePath );
	}
	
	/**
	 * Get Province Canada
	 *
	 * @return array
	 */
	public function getCanadaProvince (): array {
		return $this->province;
	}
	
	
	/**
	 * Output Wysiwyg editor
	 *
	 * @param $post_id
	 */
	public function getWysiwyg ( $post_id ) {
		$text = get_post_meta( $post_id, 'jwa_location_about', true );
		wp_editor( $text, 'jwa_about_text', [
			'textarea_name'    => 'jwa_location_about',
			'textarea_rows'    => 50,
			'teeny'            => false,
			'drag_drop_upload' => true,
			'tinymce'          => true,
			'quicktags'        => true,
		] );
	}
	
	/**
	 * Get postMeta in one Array
	 *
	 * @param array $fields
	 * @param int   $postID
	 *
	 * @return array|false
	 */
	public function getMetaField ( array $fields, int $postID ) {
		$postMeta = [];
		
		if ( empty( $fields ) ) {
			return false;
		}
		foreach ( $fields as $field ) {
			$nameField          = 'jwa_location_' . $field;
			$postMeta[ $field ] = get_post_meta( $postID, $nameField, true );
		}
		
		return $postMeta;
	}
	
	/**
	 * Get meta by Name
	 *
	 * @param string $nameField
	 * @param        $postID
	 *
	 * @return mixed
	 */
	public function getMetaByName ( string $nameField, $postID ) {
		return get_post_meta( $postID, 'jwa_location_' . $nameField, true );
	}
	
	/**
	 * Return array meta field
	 *
	 * @param array $params
	 *
	 * @return array
	 */
	public function setMetaField ( array $params, $postID = null ): array {
		unset( $params['token'] );
		
		$metaArray = [];
		
		foreach ( $params as $key => $param ) {
			switch ( $key ) {
				case 'gallery':
					if ( isset( $postID ) ) {
						$oldGallery                     = self::getMetaByName( 'gallery', $postID );
						$delete                         = self::deleteOldGalleryImage( $oldGallery );
						$metaArray["old_gallery"]       = $delete;
						$galleryID                      = self::uploadImagGallery( $param );
						$metaArray["jwa_location_$key"] = $galleryID;
					} else {
						$galleryID                      = self::uploadImagGallery( $param );
						$metaArray["jwa_location_$key"] = implode( ',', $galleryID );
					}
					break;
				case 'about':
					$metaArray["jwa_location_$key"] = ( ! empty( $params['about'] ) ? wp_filter_nohtml_kses( $param )
						: '' );
					break;
				case 'open_hours':
					$openHoursArray = [];
					foreach ( $param as $items ) {
						foreach ( $items as $keyDay => $day ) {
							$openHoursArray[ $keyDay ] = $day;
						}
					}
					$metaArray["jwa_location_$key"] = $openHoursArray;
					break;
				case'soc':
					$metaArray["jwa_location_$key"] = $param[0];
					break;
				default:
					if ( ! empty( $param ) ) {
						$metaArray["jwa_location_$key"] = $param;
					}
			}
		}
		unset( $metaArray["jwa_location_address"], $metaArray["jwa_location_postal_code"], $metaArray["jwa_location_country"], $metaArray["jwa_location_province"], $metaArray["jwa_location_lat"], $metaArray["jwa_location_lng"] );
		
		
		return $metaArray;
	}
	
	/**
	 * Delete Review
	 *
	 * @param int $idReview
	 * @param int $postID
	 *
	 * @return bool|int
	 */
	public function deleteReview ( int $idReview, int $postID ) {
		$review = self::getMetaByName( 'reviews', $postID );
		if ( ! empty( $review ) ) {
			unset( $review[ $idReview ] );
			$statusUpdate = update_post_meta( $postID, 'jwa_location_reviews', $review );
			
			return $statusUpdate;
		} else {
			return false;
		}
	}
	
	/**
	 * Set Review Status
	 *
	 * @param int    $idReview
	 * @param int    $postID
	 * @param string $status
	 *
	 * @return bool|int
	 */
	public function setReviewStatus ( int $idReview, int $postID, string $status ) {
		$review = self::getMetaByName( 'reviews', $postID );
		
		if ( ! empty( $review ) ) {
			$review[ $idReview ]['status'] = $status;
			$statusUpdate                  = update_post_meta( $postID, 'jwa_location_reviews', $review );
			
			return $statusUpdate;
		} else {
			return false;
		}
	}
	
	/**
	 * Upload Images Gallery
	 *
	 * @param array $images
	 *
	 * @return array
	 */
	public function uploadImagGallery ( array $images ) {
		$imagesID = [];
		require_once( ABSPATH . 'wp-admin/includes/image.php' );
		foreach ( $images as $key => $image ) {
			$image_url        = $image;
			$image_name       = basename( $image );
			$upload_dir       = wp_upload_dir();
			$image_data       = file_get_contents( $image_url );
			$unique_file_name = wp_unique_filename( $upload_dir['path'], $image_name );
			$filename         = basename( $unique_file_name );
			
			if ( wp_mkdir_p( $upload_dir['path'] ) ) {
				$file = $upload_dir['path'] . '/' . $filename;
			} else {
				$file = $upload_dir['basedir'] . '/' . $filename;
			}
			
			if ( is_writable( $upload_dir['path'] ) ) {
				file_put_contents( $file, $image_data );
			} else {
				$error = new \WP_Error( 'error_key', 'Permission Denied to file Upload', 403 );
				
				return $error;
			}
			
			$wp_filetype = wp_check_filetype( $filename, null );
			
			$attachment = [
				'guid'           => $upload_dir['url'] . '/' . $filename,
				'post_mime_type' => $wp_filetype['type'],
				'post_title'     => $filename,
				'post_content'   => '',
				'post_status'    => 'inherit',
			];
			
			$attach_id  = wp_insert_attachment( $attachment, $file, 0 );
			$imagesID[] = $attach_id;
			
			$attach_data = wp_generate_attachment_metadata( $attach_id, $file );
			wp_update_attachment_metadata( $attach_id, $attach_data );
		}
		
		return $imagesID;
	}
	
	/**
	 * Delete Old Image Gallery
	 *
	 * @param $galleryIDs
	 *
	 * @return array
	 */
	public function deleteOldGalleryImage ( $galleryIDs ): array {
		$delete = [];
		foreach ( $galleryIDs as $image ) {
			$delete[] = wp_delete_attachment( $image, true );
		}
		
		return $delete;
	}
	
	/**
	 * If the thumbnail is not set it takes the first gallery picture
	 *
	 * @param $galleryArray
	 * @param $postID
	 */
	public function setThumbnail ( $galleryArray, $postID ) {
		$thumbnailsID = $galleryArray;
		if ( is_array( $galleryArray ) ) {
			$set = set_post_thumbnail( $postID, (int) $thumbnailsID[0] );
		} else if ( is_string( $galleryArray ) ) {
			$set = set_post_thumbnail( $postID, (int) $thumbnailsID );
		} else {
			$galIDs = explode( ',', $galleryArray );
			$set    = set_post_thumbnail( $postID, (int) $galIDs[0] );
		}
	}
	
	/**
	 * Return Gallery Image Url
	 *
	 * @param int | string $postID
	 *
	 * @return array
	 */
	public function getGalleryLocation ( $postID ): array {
		$galleryUrl = [];
		$galleryID  = self::getMetaByName( 'gallery', (int) $postID );
		foreach ( $galleryID as $image ) {
			$galleryUrl[] = wp_get_attachment_url( $image );
		}
		
		return $galleryUrl;
	}
	
	/**
	 * Set Location Term
	 *
	 * @param $postID
	 * @param $termName
	 * @param $taxonomyName
	 * @param $parent
	 *
	 * @return array|false|\WP_Error
	 */
	public function setTerm ( $postID, $termName, $taxonomyName, $parent = null ) {
		$oldTermByTax = wp_get_post_terms( (int) $postID, $taxonomyName, [ 'fields' => 'names' ] );
		if ( ! empty( $oldTermByTax ) && $parent == null ) {
			wp_remove_object_terms( (int) $postID, $oldTermByTax, $taxonomyName );
		}
		
		if ( is_taxonomy_hierarchical( $taxonomyName ) ) {
			if ( is_array( $termName ) ) {
				$termIDs = [];
				foreach ( $termName as $name ) {
					$termIDs[] = get_term_by( 'name', sanitize_text_field( $name ), $taxonomyName )->term_id;
					if ( empty( $termIDs ) || $termIDs[0] == null ) {
						$termIDs[] = self::createNewTerm( $name, $taxonomyName );
					}
				}
				
				if ( ! empty( $termIDs ) ) {
					foreach ( $termIDs as $ID ) {
						$terms = wp_set_post_terms( (int) $postID, $ID, $taxonomyName, true );
					}
				}
				
			} else {
				
				$termID = get_term_by( 'name', sanitize_text_field( $termName ), $taxonomyName )->term_id;
				
				if ( empty( $termID ) ) {
					$ID    = self::createNewTerm( $termName, $taxonomyName, $parent );
					$terms = wp_set_post_terms( (int) $postID, [ $ID ], $taxonomyName, true );
				} else {
					$terms = wp_set_post_terms( (int) $postID, [ $termID ], $taxonomyName, true );
				}
				
			}
		} else {
			$terms = wp_set_post_terms( (int) $postID, $termName, $taxonomyName, true );
		}
		if ( ! is_wp_error( $terms ) ) {
			return $terms;
		} else {
			return [ 'error' => $terms->get_error_message(), 'code_error' => $terms->get_error_code() ];
		}
	}
	
	/**
	 * Set Term Meta
	 *
	 * @param $termID
	 * @param $metaName
	 * @param $metaValue
	 *
	 * @return bool|int|\WP_Error
	 */
	public function setTermMeta ( $termID, $metaName, $metaValue ) {
		return update_term_meta( $termID, $metaName, $metaValue );
	}
	
	/**
	 * Create New Term
	 *
	 * @param      $termName
	 * @param      $taxonomyName
	 * @param null $parent
	 *
	 * @return false|int|mixed
	 */
	public function createNewTerm ( string $termName, string $taxonomyName, $parent = null ) {
		
		if ( isset( $parent ) && term_exists( (int) $parent, $taxonomyName ) ) {
			$insert_data = wp_insert_term(
				$termName,
				$taxonomyName,
				[
					'description' => '',
					'slug'        => '',
					'parent'      => (int) $parent,
				]
			);
			
		} else {
			$insert_data = wp_insert_term(
				$termName,
				$taxonomyName,
				[
					'description' => '',
					'slug'        => '',
				]
			);
		}
		if ( ! is_wp_error( $insert_data ) && ! empty( $insert_data ) ) {
			return $insert_data['term_id'];
		}
		
		return false;
	}
	
	/**
	 * Insert or Update location data in custom table
	 *
	 * @param       $postID
	 * @param array $dataLocation
	 *
	 * @return array
	 */
	public function insertToLocationTable ( $postID, array $dataLocation ): array {
		$prefix = $this->db->prefix;
		$sql    = "SELECT * FROM `{$prefix}jwa_location` WHERE `post_id` = {$postID}";
		$update = $this->db->get_results( $sql );
		if ( empty( $update ) || empty( $update[0] ) ) {
			$response = $this->db->insert( "{$prefix}jwa_location", [
				'post_id'     => $postID,
				'address'     => $dataLocation['address'],
				'postal_code' => $dataLocation['postal_code'],
				'country'     => $dataLocation['country'],
				'province'    => $dataLocation['province'],
				'lat'         => $dataLocation['lat'],
				'lng'         => $dataLocation['lng'],
			], [ '%d', '%s', '%s', '%s', '%s', '%f', '%f' ] );
		} else {
			$response = $this->db->update( $prefix . "jwa_location", [
				'address'     => $dataLocation['address'],
				'postal_code' => $dataLocation['postal_code'],
				'country'     => $dataLocation['country'],
				'province'    => $dataLocation['province'],
				'lat'         => $dataLocation['lat'],
				'lng'         => $dataLocation['lng'],
			], [
				'id' =>
					$update[0]->id,
			], [ '%s', '%s', '%s', '%s', '%f', '%f' ] );
		}
		
		if ( $response ) {
			return [ 'success' => true, 'id_in_location_table' => $response ];
		} else {
			return [ 'success' => false, 'error' => $this->db->last_error ];
		}
	}
	
	/**
	 * Get data from jaw_location table
	 *
	 * @param $postID
	 *
	 * @return array|false
	 */
	public function getInLocationTable ( $postID ) {
		$prefix   = $this->db->prefix;
		$sql      = "SELECT * FROM `{$prefix}jwa_location` WHERE `post_id` = {$postID}";
		$location = $this->db->get_results( $sql );
		if ( ! empty( $location ) || ! empty( $location[0] ) ) {
			$locationData = [];
			foreach ( $location as $data ) {
				$locationData['address']     = $data->address;
				$locationData['postal_code'] = $data->postal_code;
				$locationData['country']     = $data->country;
				$locationData['province']    = $data->province;
				$locationData['lat']         = $data->lat;
				$locationData['lng']         = $data->lng;
			}
			
			return $locationData;
		}
		
		return false;
	}
	
	/**
	 * Get city location
	 *
	 * @param $terms
	 *
	 * @return false| object /WP_Term
	 */
	public function getLocationCity ( $terms ) {
		if ( empty( $terms ) ) {
			return false;
		}
		
		foreach ( $terms as $term ) {
			if ( $term->parent != 0 ) {
				return $term;
			}
		}
	}
	
	
}