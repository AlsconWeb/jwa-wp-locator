## Helpers  Function Class jwaPostData

<table>
    <thead>
<tr><td>Name</td><td>Parameter</td><td>Return</td><td>Description</td></tr>    
</thead>
  <tr>
    <td>versionFile</td>
    <td>string <b>filePath</b></td>
    <td>time stamp</td>
    <td>The parameter is specified from the root folder of the plugin. and returns the timestamp for the file version</td>
  </tr>
  <tr>
    <td>getCanadaProvince</td>
    <td>no</td>
    <td>associative array</td>
    <td>Returns an array of Canadian regions where key is the region's abbreviation and the value is complete</td>
  </tr>
  <tr>
    <td>getWysiwyg</td>
    <td>string | int <b>postID</b> </td>
    <td>Array</td>
    <td>Inserts the standard wordpress editor</td>
  </tr>
  <tr>
    <td>getMetaField</td>
    <td>array <b>fields</b>, int <b>postID</b> </td>
    <td>associative array</td>
    <td>Returns all post meta fields in array automatically substitutes the plugin prefix</td>
  </tr>
  <tr>
    <td>getMetaByName</td>
    <td>string <b>nameField</b>, string | int <b>postID</b></td>
    <td>associative array | string | int </td>
    <td>Will return meta field by name, automatically substitutes the plugin prefix</td>
  </tr>
  <tr>
    <td>setMetaField</td>
    <td>array <b>params</b>, int | string <b>postID</b> optional </td>
    <td>associative array, int | string  </td>
    <td>Processes an associative array of parameters and creates a post meta-key array. Key field name prefixed 
if there is a postID value, it will add new ones to the old data in such fields as reviews. Used in REST API</td>
  </tr>
  <tr>
    <td>deleteReview</td>
    <td>int <b>idReview</b>, int <b>postID</b></td>
    <td>boolean</td>
    <td>Removes feedback on index in array</td>
  </tr>
  <tr>
    <td>setReviewStatus</td>
    <td>int <b>idReview</b>, int <b>postID</b>, string <b>status</b> (publish | draft) </td>
    <td>boolean</td>
    <td>Set Review Status</td>
  </tr>
  <tr>
    <td>uploadImagGallery</td>
    <td>array <b>images</b></td>
    <td>array ID</td>
    <td>Loads pictures to the media library in WordPress, returns the ID array</td>
  </tr>
  <tr>
    <td>deleteOldGalleryImage</td>
    <td>array <b>galleryIDs</b></td>
    <td>array ID</td>
    <td>Removes the old gallery from the media library and server, returns the ID of the deleted images </td>
  </tr>
  <tr>
    <td>setThumbnail</td>
    <td>array <b>galleryArray</b>, int | string <b>postID</b> </td>
    <td>void</td>
    <td>Accepts an array of image IDs, Sets the post thumbnail to the first image from the array</td>
  </tr>
  <tr>
    <td>getGalleryLocation</td>
    <td>int | string <b>postID</b></td>
    <td>array URls </td>
    <td>Return Gallery Image Url</td>
  </tr>
  <tr>
    <td>createNewTerm</td>
    <td>string <b>termName</b>, string <b>taxonomyName</b>, optional <b>parent</b></td>
    <td>int | false</td>
    <td>Creates a new term in the specified taxonomy</td>
  </tr>
  <tr>
    <td>setTerm</td>
    <td>int | string <b>postID</b>, string <b>termName</b>, string <b>taxonomyName</b></td>
    <td>array | false | WP_Error</td>
    <td>Set Location Term</td>
  </tr>
</table>

## Helpers  Function Class jwaLocationTime

The construct accepts **Post ID** format string | int

<table>
    <thead>
      <tr><td>Name</td><td>Parameter</td><td>Return</td><td>Description</td></tr>
    </thead>
    <tr>
      <td>getTimeOpen</td>
      <td>no</td>
      <td>array | false</td>
      <td>Get Business Time location</td>
    </tr>
    <tr>
      <td>isCurrentDay</td>
      <td>string <b>dayName</b></td>
      <td>bool</td>
      <td>Compares the current date with business Time Location </td>
    </tr>
    <tr>
      <td>openCurrentDay</td>
      <td>no</td>
      <td>string | false</td>
      <td>Returns the time the location was opened on the current day</td>
    </tr>
    <tr>
      <td>insertToLocationTable</td>
      <td>string | int <b>postID</b>, array <b>dataLocation</b></td>
      <td>array</td>
      <td>Insert or Update location data in custom table</td>
    </tr>
    <tr>
      <td>getInLocationTable</td>
      <td>string | int <b>postID</b></td>
      <td>array</td>
      <td>Get data from jaw_location table</td>
    </tr>
</table>

## Helpers  Function Class jwaLocationReview

The construct accepts **Comment Data** format array | null

<table>
    <thead>
      <tr><td>Name</td><td>Parameter</td><td>Return</td><td>Description</td></tr>
    </thead>
    <tr>
      <td>insertReview</td>
      <td>no</td>
      <td>false | int | string | \WP_Error</td>
      <td>Insert Review Location</td>
    </tr>
    <tr>
      <td>changeStatus</td>
      <td>string <b>newStatus</b>, int | string <b>commentID</b></td>
      <td>bool</td>
      <td>Changes the status of the store's rating so that it is calculated in the overall rating</td>
    </tr>
    <tr>
      <td>getAvatarUser</td>
      <td>string | int <b>commentID</b></td>
      <td>false|int|string</td>
      <td>Return URL Avatar user</td>
    </tr>
    <tr>
      <td>getCommentData</td>
      <td>string | int <b>commentID</b></td>
      <td>string | false</td>
      <td>Returns the date a review was posted</td>
    </tr>
    <tr>
      <td>getStarRating</td>
      <td>string | int <b>postID</b></td>
      <td>float</td>
      <td>Get star rating location</td>
    </tr>
    <tr>
      <td>getRatingChartData</td>
      <td>string | int <b>postID</b></td>
      <td>array</td>
      <td>Returns an array of data for Rating Chart </td>
    </tr>
    <tr>
      <td>getPercentToProgressBar</td>
      <td>int <b>total</b> int <b>value</b></td>
      <td>float | int</td>
      <td>Will return the percentage of progress</td>
    </tr>
    <tr>
      <td>getRatingInComments</td>
      <td>int | string <b>commentID</b></td>
      <td>int</td>
      <td>User rating in the review</td>
    </tr>
</table>

## Rest API

**Get information about a location Method GET endpoint**  `example.com/wp-json/jwa-locator/v1/location/(post id)`

Answer: json object

`{
"title": "Aylmer Nelson Cannabis",
"status": "publish",
"gallery": [],
"category": [
"dispensary"
],
"tag": "",
"phone": "403-766-9141",
"web_site": "aylmernelson.com",
"open_hours": {
"thu": {
"open": "10:00",
"close": "20:00"
},
"fri": {
"open": "10:00",
"close": "20:00"
},
"sat": {
"open": "10:00",
"close": "20:00"
},
"sun": {
"open": "10:00",
"close": "20:00"
},
"mon": {
"open": "10:00",
"close": "18:00"
},
"tue": {
"open": "10:00",
"close": "20:00"
},
"wed": {
"open": "10:00",
"close": "20:00"
} },
"soc": "",
"about": "",
"general_info": {
"address": "Aylmer Nelson Cannabis, 1309 9 Ave SE, Calgary, AB T2G 0T3, Canada",
"postal_code": "T2G 0T3",
"country": "",
"province": "",
"lat": "51.0409",
"lng": "-114.034"} }`

**Get information reviews method GET endpoint** `example.com/wp-json/jwa-locator/v1/location/review/(post id)`

Answer: array json object

`[
{
"comment_id": "125",
"post_id": "7752",
"author": "adm_mag",
"comment_date": "2021-03-09 11:08:44",
"comment_content": "Comment by the login user  ",
"comment_parent": "0",
"avatar": "https:\/\/i.pravatar.cc\/64",
"rating": "5"
}, {
"comment_id": "124",
"post_id": "7752",
"author": "test by alex",
"comment_date": "2021-03-09 11:07:32",
"comment_content": "Test comment by insert to table",
"comment_parent": "0",
"avatar": "https:\/\/i.pravatar.cc\/64",
"rating": "4"
}
]`

**Insert location method POST endpoint** `example.com/wp-json/jwa-locator/v1/location/`

Content-Type: application/json

* token => string (required)
* title => string (required)
* address => string (required)
* postal_code => string
* lat => float
* lng => float
* city => array object
  * name => string
  * lat => float
  * lng => float

* country => string 2 letter (ISO 3166)
* status => publish | draft
* province => array object
  * name => string 2 letter (ISO 3166)
  * lat => float
  * lng => float
* phone => string mask ("647-313-6612")
* web_site => string
* open_hours => array object
  * mon => object
    * open => string
    * close => string

* logo => string url
* soc => array object
  * fb => string url
  * tw => string url
  * in => string url

* gallery => array urls
* about => string
* category => array strings
* tag => array strings

Answer: array json object

`{
"statusCode": 200,
"locationID": 7792,
"set_category": [
"677"
],
"set_tag": [
"686",
"687",
"688"
],
"set_city": [
"2403"
],
"set_provice": [
"4109"
],
"location_table": {
"success": true,
"id_in_location_table": 1 },
"link": "https:\/\/thompson.justwebagency.com\/magazine\/locator\/all-access-medical-on-204-baldwin-street\/"
}`

**Update location method POST endpoint** `example.com/wp-json/jwa-locator/v1/location/post_id`

See parameters above

Answer: array json object

`{
"status": 200,
"category": [
"677"
],
"tag": [
"686",
"687",
"688"
],
"city": [
"2403"
],
"link": "https:\/\/thompson.justwebagency.com\/magazine\/?post_type=locator&p=7792"
}`

**Add reviews method POST endpoint** `example.com/wp-json/jwa-locator/v1/location/review/post_id`

Content-Type: application/json

* token => string (required)
* reviews => array object
  * text => string
  * date => string format 2020-03-22
  * reviewer => string
  * rete => int
  * status => draft | publish
  * avatar => url avatar
  * email => email

Answer: array json object

`{
"comment_id": [
126, 127, 128
]
}`

All inquiries can be found Test->RESR->test.http file