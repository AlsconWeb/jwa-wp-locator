jQuery(document).ready(function ($) {
	
	/**
	 * init bootstrap tabs
	 *
	 * @type {*[]}
	 */
	let triggerTabList = [].slice.call(document.querySelectorAll('#pills-tab a'))
	triggerTabList.forEach(function (triggerEl) {
		let tabTrigger = new bootstrap.Tab(triggerEl)
		
		triggerEl.addEventListener('click', function (event) {
			event.preventDefault()
			tabTrigger.show()
		})
	})
	
	
	let map;
	let marker;
	
	if ($('#jwa_location_lat').val() && $('#jwa_location_lng').val()) {
		let lat = $('#jwa_location_lat').val();
		let lng = $('#jwa_location_lng').val();
		
		map = new google.maps.Map(document.getElementById("map"), {
			center: {lat: parseFloat(lat), lng: parseFloat(lng)},
			zoom: 8,
		});
		const marker = new google.maps.Marker({
			map,
			anchorPoint: new google.maps.Point(0, -29),
		});
		map.setZoom(17); // Why 17? Because it looks good.
		marker.setPosition({lat: parseFloat(lat), lng: parseFloat(lng)});
		marker.setVisible(true);
		
	} else {
		map = new google.maps.Map(document.getElementById("map"), {
			center: {lat: 43.7184034, lng: -79.5184821},
			zoom: 8,
		});
	}
	
	/**
	 * Place Autocomplete
	 */
	const input = document.getElementById("jwa_location_address");
	const autocomplete = new google.maps.places.Autocomplete(input);
	
	autocomplete.bindTo("bounds", map);
	autocomplete.setFields(["address_components", "geometry", "icon", "name"]);
	autocomplete.addListener("place_changed", () => {
		
		// marker.setVisible(false);
		const place = autocomplete.getPlace();
		if (!place.geometry) {
			window.alert("No details available for input: '" + place.name + "'");
			return;
		}
		// If the place has a geometry, then present it on a map.
		if (place.geometry.viewport) {
			map.fitBounds(place.geometry.viewport);
		} else {
			map.setCenter(place.geometry.location);
			map.setZoom(17); // Why 17? Because it looks good.
		}
		
		$('#jwa_location_lat').val(place.geometry.location.lat());
		$('#jwa_location_lng').val(place.geometry.location.lng());
		let city;
		let province;
		for (let i = 0; i <= place.address_components.length - 1; i++) {
			if (place.address_components[i].types && place.address_components[i].types[0] == "locality") {
				city = place.address_components[i].long_name
			}
			
			if (place.address_components[i].types && place.address_components[i].types[0] == "administrative_area_level_1") {
				province = place.address_components[i].short_name.toLowerCase();
			}
		}
		console.log(province)
		$('#jwa_location_city').val(city);
		$('#jwa_location_province').val(province.toUpperCase());
		
		marker = new google.maps.Marker({
			position: location,
			map: map,
		});
		marker.setPosition({lat: place.geometry.location.lat(), lng: place.geometry.location.lng()});
		
		marker.setPosition(place.geometry.location);
		marker.setVisible(true);
		let address = "";
		console.log(place)
		if (place.address_components) {
			address = [
				(place.address_components[0] &&
					place.address_components[0].short_name) ||
				"",
				(place.address_components[1] &&
					place.address_components[1].short_name) ||
				"",
				(place.address_components[2] &&
					place.address_components[2].short_name) ||
				"",
			].join(" ");
			
			if (place.address_components[7].short_name) {
				$('#jwa_location_postal_code').val(place.address_components[7].short_name);
			} else {
				$('#jwa_location_postal_code').val('');
			}
			
			if (place.address_components[5].short_name) {
				let option = $('#jwa_location_province option');
				$.each(option, function (i, val) {
					if ($(val).val() == place.address_components[5].short_name) {
						$(option[i]).attr('selected', 'selected');
					}
				});
			}
			
		}
		
	});
	
	/**
	 * Upload Images Gallery
	 */
	$('body').on('click', '#jwa_upload', function (e) {
		
		e.preventDefault();
		
		let button = $(this),
			custom_uploader = wp.media({
				title: 'Insert image',
				library: {
					// uploadedTo : wp.media.view.settings.post.id, // attach to the current post?
					type: 'image'
				},
				button: {
					text: 'Use this image' // button label text
				},
				multiple: true
			}).on('select', function () { // it also has "open" and "close" events
				let attachment = custom_uploader.state().get('selection').toJSON();
				let galleryArray = [];
				$.each(attachment, function (i, val) {
					let html = '<div class="col-sm-12 col-md-12 col-lg-4 col-xl-4"><a href="#" class="btn btn-secondary' +
						' delete-image" data-index=""><svg xmlns="http://www.w3.org/2000/svg" width="16"' +
						' height="16" fill="currentColor" class="bi bi-x-square" viewBox="0 0 16 16"><path d="M14 1a1 1 0 0 1 1' +
						' 1v12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h12zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0' +
						' 2-2V2a2 2 0 0 0-2-2H2z"/><path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/></svg></a><img src="' + val.url + '" class="img-thumbnail" style="width: 33%;height:auto;" alt="gallery"></div>';
					$('.card-group').append(html);
					galleryArray.push(val.id);
				});
				reIndex()
				let oldID = [];
				
				if ($('#jwa_location_gallery').val()) {
					oldID = [$('#jwa_location_gallery').val()];
				}
				oldID.push(galleryArray);
				$('#jwa_location_gallery').val(oldID);
			}).open();
		
	});
	
	/**
	 * Remove all gallery, remove post meta
	 */
	$('#jwa_remove_all').on('click', function (e) {
		e.preventDefault();
		$.ajax({
			type: 'POST',
			url: '',
			data: {
				action: 'jwa_remove_gallery',
				post_id: $('#post_id').val()
			},
			success: function (res) {
				$('#gallery_id').val();
				$('.image-gallery-metabox').remove();
			},
			error: function (xhr, ajaxOptions, thrownError) {
			
			},
		});
	});
	
	/**
	 * Remove image
	 */
	const gallerysID = $('#jwa_location_gallery').val().split(',');
	
	$('.delete-image').click(function (e) {
		e.preventDefault();
		let index = $(this).data('index');
		$(this).parent().remove();
		gallerysID.splice(index, 1);
		console.log('gallerysID', gallerysID, index)
		$('#jwa_location_gallery').val(gallerysID)
	});
	
	/**
	 * reindex image
	 */
	function reIndex() {
		let btn = $('.delete-image')
		$.each(btn, function (i, val) {
				$(this).data('index', i);
			}
		)
		;
	}
	
	/**
	 * Ajax change status review
	 */
	$('.card input[type=checkbox]').change(function (e) {
		let id = $(this).parent().parent().parent().data('id');
		let postID = $(this).parent().parent().parent().data('post_id');
		let data = {
			action: 'set_review_status',
			id: id,
			postID: postID,
			status: $(this).val(),
		};
		
		$.ajax({
			type: 'POST',
			url: jwa_locator.url_ajax,
			data: data,
			success: function (res) {
				console.log(res)
			},
			error: function (xhr, ajaxOptions, thrownError) {
				console.log('error...', xhr);
				//error logging
			},
		});
	});
	
	/**
	 * Change status review by click
	 */
	$('.card input[type=checkbox]').click(function (e) {
		$(this).val(($(this).val() == 'publish' ? 'draft' : 'publish'));
	});
	
	/**
	 * Ajax delete review
	 */
	$('.delete-review').click(function (e) {
		e.preventDefault();
		let el = $(this).parent().parent();
		let postID = $(this).parent().parent().data('post_id');
		let id = $(this).parent().parent().data('id');
		let data = {
			action: 'delete_review',
			postID: postID,
			id: id
		};
		
		$.ajax({
			type: 'POST',
			url: jwa_locator.url_ajax,
			data: data,
			success: function (res) {
				if (res.data.status) {
					el.remove();
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				console.log('error...', xhr);
				//error logging
			},
		});
		
	});
	
	
});