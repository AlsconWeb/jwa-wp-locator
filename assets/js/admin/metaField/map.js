jQuery(document).ready(function ($) {
	let map;
	let marker;
	
	if ($('#jwa-location-lat').val() && $('#jwa-location-lng').val()) {
		let lat = $('#jwa-location-lat').val();
		let lng = $('#jwa-location-lng').val();
		let coordinate = new google.maps.LatLng(lat, lng)
		map = new google.maps.Map(document.getElementById("map-in-city"), {
			center: coordinate,
			zoom: 8,
		});
		const marker = new google.maps.Marker({
			map,
			anchorPoint: new google.maps.Point(0, -29),
		});
		map.setZoom(17); // Why 17? Because it looks good.
		
		marker.setPosition(coordinate);
		marker.setVisible(true);
		
	} else {
		map = new google.maps.Map(document.getElementById("map-in-city"), {
			center: {lat: 43.7184034, lng: -79.5184821},
			zoom: 8,
		});
	}
	
	
	/**
	 * Place Autocomplete
	 */
	const input = document.getElementById("jwa_location_city");
	const autocomplete = new google.maps.places.Autocomplete(input);
	
	autocomplete.bindTo("bounds", map);
	autocomplete.setFields(["address_components", "geometry", "icon", "name"]);
	autocomplete.addListener("place_changed", () => {
		
		// marker.setVisible(false);
		const place = autocomplete.getPlace();
		if (!place.geometry) {
			window.alert("No details available for input: '" + place.name + "'");
			return;
		}
		// If the place has a geometry, then present it on a map.
		if (place.geometry.viewport) {
			map.fitBounds(place.geometry.viewport);
		} else {
			map.setCenter(place.geometry.location);
			map.setZoom(17); // Why 17? Because it looks good.
		}
		
		$('#jwa-location-lat').val(place.geometry.location.lat());
		$('#jwa-location-lng').val(place.geometry.location.lng());
		
		marker = new google.maps.Marker({
			position: place.geometry.location,
			map: map,
		});
		
		// marker.setPosition(place.geometry.location);
		marker.setPosition(place.geometry.location);
		marker.setVisible(true);
	});
});