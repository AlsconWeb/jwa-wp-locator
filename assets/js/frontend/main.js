jQuery(document).ready(function ($) {
	/**
	 * select star
	 */
	$('.reviews-form .rating li').click(function (e) {
		let star = $('.reviews-form .rating li');
		let currentStar = Number($(this).data('index'));
		$('#rate').val(currentStar)
		star.removeClass('star');
		$.each(star, function (i, el) {
			if (currentStar > i) {
				$(el).addClass('star');
			}
		});
	});
	
	/**
	 * Ajax comment send
	 */
	$('#comment_dis').submit(function (e) {
		e.preventDefault();
		let data = {
			action: "jwa_location_reviews",
			content: $('#comment_dis textarea').val(),
			post_id: $('#comment_dis #post_id').val(),
			user_id: $('#comment_dis #user_id').val(),
			rete: $('#comment_dis #rate').val(),
			user_name: $('#comment_dis #name').val(),
			user_email: $('#comment_dis #email').val(),
		}
		
		$.ajax({
			type: 'POST',
			url: jwaLocation.ajax,
			data: data,
			success: function (res) {
				console.log(res)
				if (res.success) {
					Swal.fire({
						icon: 'success',
						title: res.data.message,
						willClose: () => {
							location.reload();
						}
					})
				} else {
					Swal.fire({
						icon: 'error',
						title: res.data.message,
					})
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				console.log('error...', xhr);
				//error logging
			},
		});
	});
	
	
});