/*MAP*/
jQuery(document).ready(function ($) {
	/**
	 * Draft markers
	 * @type {google.maps.OverlayView}
	 */
	CustomMarker.prototype = new google.maps.OverlayView();
	
	function CustomMarker(opts) {
		this.setValues(opts);
	}
	
	CustomMarker.prototype.draw = function () {
		
		var self = this,
			div = this.div;
		if (!div) {
			div = this.div = $('' + '<div class="marker-map ' + self.markerClass + '">' + '<div class="shadow"></div>' + '<div class="pulse" ></div>' + '<div class="pin-wrap">' + '<div class="pin"></div>' + '</div>' + '</div>' + '')[0];
			this.pinWrap = this.div.getElementsByClassName('pin-wrap');
			this.pin = this.div.getElementsByClassName('pin');
			this.pinShadow = this.div.getElementsByClassName('shadow');
			div.style.position = 'absolute';
			div.style.cursor = 'pointer';
			var panes = this.getPanes();
			panes.overlayImage.appendChild(div);
		}
		var point = this.getProjection().fromLatLngToDivPixel(this.position);
		if (point) {
			div.style.left = point.x + 'px';
			div.style.top = point.y + 'px';
		}
	};
	CustomMarker.prototype.animateDrop = function () {
		dynamics.stop(this.pinWrap);
		dynamics.css(this.pinWrap, {
			'transform': 'scaleY(2) translateY(-' + $('#map').outerHeight() + 'px)',
			'opacity': '1'
		});
		dynamics.animate(this.pinWrap, {
			translateY: 0,
			scaleY: 1.0
		}, {
			type: dynamics.gravity,
			duration: 1800
		});
		dynamics.stop(this.pin);
		dynamics.css(this.pin, {
			'transform': 'none'
		});
		dynamics.animate(this.pin, {
			scaleY: 0.8
		}, {
			type: dynamics.bounce,
			duration: 1800,
			bounciness: 600
		});
		dynamics.stop(this.pinShadow);
		dynamics.css(this.pinShadow, {
			'transform': 'scale(0,0)'
		});
		dynamics.animate(this.pinShadow, {
			scale: 1
		}, {
			type: dynamics.gravity,
			duration: 1800
		});
	}
	CustomMarker.prototype.animateBounce = function () {
		dynamics.stop(this.pinWrap);
		dynamics.css(this.pinWrap, {
			'transform': 'none'
		});
		dynamics.animate(this.pinWrap, {
			translateY: -30
		}, {
			type: dynamics.forceWithGravity,
			bounciness: 0,
			duration: 500,
			delay: 150
		});
		dynamics.stop(this.pin);
		dynamics.css(this.pin, {
			'transform': 'none'
		});
		dynamics.animate(this.pin, {
			scaleY: 0.8
		}, {
			type: dynamics.bounce,
			duration: 800,
			bounciness: 0
		});
		dynamics.animate(this.pin, {
			scaleY: 0.8
		}, {
			type: dynamics.bounce,
			duration: 800,
			bounciness: 600,
			delay: 650
		});
		dynamics.stop(this.pinShadow);
		dynamics.css(this.pinShadow, {
			'transform': 'none'
		});
		dynamics.animate(this.pinShadow, {
			scale: 0.6
		}, {
			type: dynamics.forceWithGravity,
			bounciness: 0,
			duration: 500,
			delay: 150
		});
	};
	CustomMarker.prototype.animateWobble = function () {
		dynamics.stop(this.pinWrap);
		dynamics.css(this.pinWrap, {
			'transform': 'none'
		});
		dynamics.animate(this.pinWrap, {
			rotateZ: -45
		}, {
			type: dynamics.bounce,
			duration: 1800
		});
		dynamics.stop(this.pin);
		dynamics.css(this.pin, {
			'transform': 'none'
		});
		dynamics.animate(this.pin, {
			scaleX: 0.8
		}, {
			type: dynamics.bounce,
			duration: 800,
			bounciness: 1800
		});
	};
	
	
	var markers = [];
	var mapLat = $('#map').data('lat');
	var mapLng = $('#map').data('lng');
	$('#jwa-location-lat').val(mapLat)
	$('#jwa-location-lng').val(mapLng)
	let bounds = new google.maps.LatLngBounds();
	
	
	var myLatlng = new google.maps.LatLng(Number(mapLat), Number(mapLng));
	var map = new google.maps.Map(document.getElementById('map'), {
		zoom: 10,
		center: myLatlng,
		position: myLatlng,

	});
	$('#reviews .rating ul').each(function(){
		var lengStar = $(this).data('rating');
		$('#reviews .rating li:nth-of-type(-n+'+lengStar+'').addClass('star');
	});
	$('.results-block .results-head').click(function(){
		$(this).parents('.results').toggleClass('open')
	});
	function allMarkers() {
		if ($('.results-body .item, .dispensary .item').length > 0) {
			$('.results-body .item, .dispensary .item').each(function () {
				let mapLat = $(this).data('lat');
				let mapLng = $(this).data('lng');
				let mapType = $(this).data('type');
				markers.push({
					position: new google.maps.LatLng(mapLat, mapLng),
					map: map,
					markerClass: mapType,
					zoom: 10,
		

				});
			});
		}
	}
	
	function markersEl() {
		if ($('.results-body .item, .dispensary .item').length > 0) {
			var bounds = new google.maps.LatLngBounds();
			
			markers.forEach(function (el, i) {
				let marker = new CustomMarker(el);
			});
			for (var i = 0; i < markers.length; i++) {
				bounds.extend(markers[i].position);
			}
			map.fitBounds(bounds);
		}
		
	}
	
	init();
	
	/**
	 * Init Page
	 */
	function init() {
		selectSortBy();
		allMarkers();
		markersEl();
		updateEventListing();
	}
	
	/**
	 * Open select Sort BY
	 */
	function selectSortBy() {
		$(document).mouseup(function (e) {
			var folder = $('.select');
			if (!folder.is(e.target) && folder.has(e.target).length === 0) {
				folder.find('.select-options').slideUp();
			}
		});
		$('.sort-filter > a').click(function () {
			$(this).parent().toggleClass('open')
			$(this).next().slideToggle();
		})
		if ($(window).width() < 992) {
			if ($('.results').length) {
				$('body').css('margin-bottom', '58px');
			}
		}
		if ($(window).width() < 768) {
			$('.select p').click(function () {
				$(this).parent().find('.mobile').slideToggle();
			})
		} else {
			$('.select p').click(function () {
				$(this).next().slideToggle();
			});
		}
	}
	
	/**
	 * Place Autocomplete
	 */
	if ($('#jwa-location-address').length) {
		const input = document.getElementById("jwa-location-address");
		const autocomplete = new google.maps.places.Autocomplete(input);
		google.maps.event.addListener(map, 'bounds_changed', function () {
			autocomplete.bindTo("bounds", map);
		});
		autocomplete.setFields(["address_components", "geometry", "icon", "name"]);
		autocomplete.addListener("place_changed", () => {
			const place = autocomplete.getPlace();
			if (!place.geometry) {
				searchPlaceByText(place.name);
				return;
			}
			
			let city;
			let province;
			for (let i = 0; i <= place.address_components.length - 1; i++) {
				if (place.address_components[i].types && place.address_components[i].types[0] == "locality") {
					city = place.address_components[i].long_name
				}
				
				if (place.address_components[i].types && place.address_components[i].types[0] == "administrative_area_level_1") {
					province = place.address_components[i].short_name.toLowerCase();
				}
			}
			console.log('lan', place.geometry.location.lat(), 'lng', place.geometry.location.lng())
			$('#jwa-location-lat').val(place.geometry.location.lat());
			$('#jwa-location-lng').val(place.geometry.location.lng());
			$('#jwa-location-city').val(city);
			$('#jwa-location-province').val(province);
			// console.log(city, province)
			$('.filters').submit();
			// ajaxSearch();
		});
	}
	
	/**
	 * Ajax Search
	 *
	 * @TODO Add Preloader
	 */
	function ajaxSearch() {
		let data = {
			action: "jwa_get_location_by_coordinate",
			lat: $('#jwa-location-lat').val(),
			lng: $('#jwa-location-lng').val(),
			address: $('#jwa-location-address').val(),
			distance: $('#jwa-location-distance').val()
		}
		
		$.ajax({
			type: 'POST',
			url: jwaLocation.ajax,
			data: data,
			success: function (res) {
				let items = res.data.content
				$('.results-body .item').remove();
				if (res.data.code == 200) {
					$('.results-head .end').text(res.data.count_post);
					
					$.each(items, function (i, item) {
						$('.results-body').append(item);
					});
					updateMapFromList(map);
					updateEventListing();
				} else {
					let html = `<div class="no-found dfr"><div class="description"><p>${res.data.content}</p></div></div>`
					$('.results-body').append(html);
				}
				allMarkers();
				markersEl();
			},
			error: function (xhr, ajaxOptions, thrownError) {
				console.log('error...', xhr);
				//error logging
			}
		});
	}
	
	/**
	 * Update Event Click to Location listing
	 * @TODO Add Preloader
	 */
	function updateEventListing() {
		$('.results-body .item').click(function (e) {
			e.preventDefault();
			
			let data = {
				action: "get_location_info",
				id: $(this).data('id')
			}
			let location = $(this);
			
			$.ajax({
				type: 'POST',
				url: jwaLocation.ajax,
				data: data,
				success: function (res) {
					let item = res.data.content
					$('.results-description').remove();
					$('.results-dispensaries').append(item);
					updateMapFromLocationInfo(map, location)
					showHideLocationInfo();
				},
				error: function (xhr, ajaxOptions, thrownError) {
					console.log('error...', xhr);
					//error logging
				}
			});
			
			
		});
	}
	
	/**
	 * Open more info adn back to listing
	 */
	function showHideLocationInfo() {
		let infoLocation = $('.results-dispensaries');
		let listLocation = $('.results-block');
		
		infoLocation.show();
		listLocation.hide();
		
		$('.results-head .back').click(function (e) {
			e.preventDefault();
			infoLocation.hide();
			listLocation.show();
			updateMapFromList(map)
		});
	}
	
	/**
	 * Update map after search result
	 * @param map google map
	 */
	function updateMapFromList(map) {
		markers = []; // clean markers on map
		let firstLocation = $('.results-body .item:first-child');
		let newCenter = new google.maps.LatLng(firstLocation.data('lat'), firstLocation.data('lng'));
		$('#jwa-location-lat').val(firstLocation.data('lat'))
		$('#jwa-location-lng').val(firstLocation.data('lng'))
		map.zoom = 10;
		map.setCenter(newCenter)
	}
	
	/**
	 * Set new Center map in location info
	 * @param map
	 * @param location
	 */
	function updateMapFromLocationInfo(map, location) {
		let newCenter = new google.maps.LatLng($(location).data('lat'), $(location).data('lng'));
		map.zoom = 18;
		map.setCenter(newCenter)
	}
	
	/**
	 * Submit from by select button
	 */
	$('.filters .checkbox-button input').click(function (e) {
		setTimeout(function () {
			$('.filters').submit();
		}, 100)
	});
	
	
	if ($('.filters').length) {
		
		/**
		 * Synchronization all checkbox elements adn
		 */
		$('[type="checkbox"]').change(function () {
			var idEl = $(this).data('id');
//				nameEl = idEl.slice(0, idEl.indexOf('-'));
			if ($(this).hasClass('checked')) {
				$('[data-id*="' + idEl + '"]').prop('checked', false).removeClass('checked')
			} else {
				$('[data-id*="' + idEl + '"]').prop('checked', true).addClass('checked')
			}
		});
		
		/**
		 * Send form by search icon
		 */
		$('.filters .icon-search, .checkbox').click(function (e) {
			setTimeout(function () {
				$('.filters').submit();
			}, 100)
		});
	}
	
	function searchPlaceByText(text) {
		let place = encodeURI(text);
		let url = `https://maps.google.com/maps/api/geocode/json?address=${place}&sensor=false&language=en&key=${jwaLocation.key}`
		$.ajax({
			type: 'GET',
			url: url,
			success: function (res) {
				// do something with ajax data
				console.log(res.results[0])
				$('#jwa-location-lat').val(res.results[0].geometry.location.lat);
				$('#jwa-location-lng').val(res.results[0].geometry.location.lng);
				let city;
				let province;
				for (let i = 0; i <= res.results[0].address_components.length - 1; i++) {
					if (res.results[0].address_components[i].types && res.results[0].address_components[i].types[0] == "locality") {
						city = res.results[0].address_components[i].long_name
					}
					
					if (res.results[0].address_components[i].types && res.results[0].address_components[i].types[0] == "administrative_area_level_1") {
						province = res.results[0].address_components[i].short_name.toLowerCase();
					}
				}
				$('#jwa-location-city').val(city);
				$('#jwa-location-province').val(province);
				$('.filters').submit();
			},
			error: function (xhr, ajaxOptions, thrownError) {
				console.log('error...', xhr);
				//error logging
			},
		});
	}
	
	
});

