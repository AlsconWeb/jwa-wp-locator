<?php
/**
 * JWAUserInfoTest class file.
 *
 * @package lovik\jwa_wp_locator
 */

namespace JWA_Locator\Tests;

use JWA_Locator\Helpers\jwaUserInfo;
use Mockery;
use tad\FunctionMocker\FunctionMocker;
use WP_Error;
use WP_Mock;

/**
 * Class MainTest
 *
 * @group helpers
 */
class JWAUserInfoTest extends JWAUserInfoTestCase {

	/**
	 * Test getLocationByIP().
	 *
	 * @param string|null $remoteAddr Remote addr.
	 * @param string|null $ip         IP.
	 * @param array|false $expected   Expected value.
	 *
	 * @dataProvider dptestGetLocationByIP
	 * @throws \ReflectionException ReflectionException.
	 */
	public function testGetLocationByIP( $remoteAddr, $ip, $expected ) {
		$subject = new jwaUserInfo( $remoteAddr );

		$content = serialize( $expected );

		FunctionMocker::replace(
			'file_get_contents',
			function ( $filename ) use ( $content, $ip ) {
				if ( 'http://www.geoplugin.net/php.gp?ip=' . $ip === $filename ) {
					return $content;
				}

				return null;
			}
		);

		self::assertSame( $expected, $subject->getLocationByIP( $ip ) );

		if ( is_array( $expected ) ) {
			self::assertSame( $ip, $this->get_protected_property( $subject, 'ip' ) );
		}
	}

	/**
	 * Data provider for testGetLocationByIP().
	 *
	 * @return array
	 */
	public function dptestGetLocationByIP(): array {
		$expected = [ 'some array' ];

		return [
			'Remote addr is null'  => [ null, '1.1.1.1', false ],
			'Remote addr is empty' => [ '', '1.1.1.1', false ],
			'IP is null'           => [ '8.8.8.8', null, false ],
			'Real IP'              => [ '8.8.8.8', '', $expected ],
		];
	}

	/**
	 * Test getDefaultCityByUser().
	 *
	 * @param string|null $city          City.
	 * @param string|null $country       Country.
	 * @param string      $expected_city Expected value.
	 *
	 * @dataProvider dptestGetDefaultCityByUser
	 */
	public function testGetDefaultCityByUser( $city, $country, string $expected_city ): void {
		$remoteAddr   = '8.8.8.8';
		$locationData = [];

		if ( $city ) {
			$locationData['geoplugin_city'] = $city;
		}

		if ( $country ) {
			$locationData['geoplugin_countryCode'] = $country;
		}

		$expected = [ 'some city data' ];

		$mock = Mockery::mock( jwaUserInfo::class, [ $remoteAddr ] )->makePartial()
		               ->shouldAllowMockingProtectedMethods();
		$mock->shouldReceive( 'getLocationByIP' )->with( $remoteAddr )->andReturn( $locationData );
		$mock->shouldReceive( 'getCityData' )->with( $expected_city )->andReturn( $expected );

		self::assertSame( $expected, $mock->getDefaultCityByUser() );
	}

	/**
	 * Data provider for testGetDefaultCityByUser().
	 *
	 * @return array
	 */
	public function dptestGetDefaultCityByUser(): array {
		return [
			'no city, no country'    => [ null, null, 'new-york' ],
			'no city, bad country'   => [ null, 'RU', 'new-york' ],
			'no city, CA'            => [ null, 'CA', 'toronto' ],
			'no city, US'            => [ null, 'US', 'new-york' ],
			'some city, no country'  => [ 'some city', null, 'some city' ],
			'some city, bad country' => [ 'some city', 'RU', 'some city' ],
			'some city, CA'          => [ 'some city', 'CA', 'some city' ],
			'some city, US'          => [ 'some city', 'US', 'some city' ],
		];
	}

	/**
	 * Test getTermByIP().
	 *
	 * @param string|null  $city     City.
	 * @param object|false $expected Expected value.
	 *
	 * @dataProvider dptestGetTermByIP
	 */
	public function testGetTermByIP( $city, $expected ): void {
		$remoteAddr   = '8.8.8.8';
		$locationData = [];
		if ( $city ) {
			$locationData = [ 'geoplugin_city' => $city ];
		}

		$mock = Mockery::mock( jwaUserInfo::class, [ $remoteAddr ] )->makePartial();
		$mock->shouldReceive( 'getLocationByIP' )->with( $remoteAddr )->andReturn( $locationData );

		WP_Mock::userFunction( 'get_term_by' )->with( 'name', $city, 'area' )
		       ->andReturn( $expected );

		self::assertSame( $expected, $mock->getTermByIP() );
	}

	/**
	 * Data provider for dptestGetTermByIP().
	 *
	 * @return array
	 */
	public function dptestGetTermByIP(): array {
		$term = (object) [ 'term_id' => 23 ]; // WP_Term.

		return [
			'city'    => [ 'Toronto', $term ],
			'no_city' => [ null, false ],
		];
	}

	/**
	 * Test getCityData().
	 *
	 * @param object|false $cityTerm    City term.
	 * @param boolean|null $is_wp_error Is WP_Error.
	 * @param array|false  $expected    Expected value.
	 *
	 * @dataProvider dptestGetCityData
	 */
	public function testGetCityData( $cityTerm, $is_wp_error, $expected ) {
		$remoteAddr = '8.8.8.8';
		$city       = 'some city';

		$mock = Mockery::mock( jwaUserInfo::class, [ $remoteAddr ] )->makePartial();

		WP_Mock::userFunction( 'get_term_by' )->with( 'slug', $city, 'area' )->andReturn( $cityTerm );

		WP_Mock::userFunction( 'is_wp_error' )->with( $cityTerm )->andReturn( $is_wp_error );

		if ( $cityTerm && ! $is_wp_error ) {
			WP_Mock::userFunction( 'get_term_meta' )->with( $cityTerm->term_id, 'jwa_location_lat', true )->andReturn( 'some lat' );
			WP_Mock::userFunction( 'get_term_meta' )->with( $cityTerm->term_id, 'jwa_location_lng', true )->andReturn( 'some lng' );
		}

		self::assertSame( $expected, $mock->getCityData( $city ) );
	}

	/**
	 * Data provider for testGetCityData().
	 *
	 * @return array
	 */
	public function dptestGetCityData() {
		$name     = 'some city';
		$term_id  = 23;
		$cityTerm = (object) [
			'name'    => $name,
			'term_id' => $term_id,
		];

		$cityData = [
			'lat'     => 'some lat',
			'lng'     => 'some lng',
			'name'    => $name,
			'city_id' => $term_id,
		];

		return [
			'wrong city' => [ false, null, false ],
			'error'      => [ Mockery::mock( WP_Error::class ), true, false ],
			'good city'  => [ $cityTerm, false, $cityData ],
		];
	}
}
