<?php
/**
 * Bootstrap file for JWA WP Locator phpunit tests.
 *
 * @package lovik\jwa-wp-locator
 */

use tad\FunctionMocker\FunctionMocker;

/**
 * Plugin main file.
 */
define( 'PLUGIN_MAIN_FILE', __DIR__ . '/../../jwaWPLocator.php' );

/**
 * Plugin path.
 */
define( 'PLUGIN_PATH', realpath( dirname( PLUGIN_MAIN_FILE ) ) );

require_once PLUGIN_PATH . '/vendor/autoload.php';

if ( ! defined( 'ABSPATH' ) ) {
	/**
	 * WordPress ABSPATH.
	 */
	define( 'ABSPATH', PLUGIN_PATH . '/../../../' );
}

FunctionMocker::init(
	array(
		'blacklist'             => array(
			realpath( PLUGIN_PATH ),
		),
		'whitelist'             => array(
			realpath( PLUGIN_PATH . '/jwaWPLocator.php' ),
			realpath( PLUGIN_PATH . '/includes' ),
			realpath( PLUGIN_PATH . '/tests/PHPUnit/Stubs' ),
		),
		'redefinable-internals' => array(
			'file_get_contents',
		),
	)
);

\WP_Mock::bootstrap();
